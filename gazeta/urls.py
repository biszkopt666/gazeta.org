from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.csrf import ensure_csrf_cookie

urlpatterns = [
    path('api/accounts/', include('accounts.urls')),
    path('api/articles/', include('articles.urls')),
    #Adios because ADBLOCK hates advertisements :-)
    path('api/adios/', include('advertisements.urls')),
    path('api/photos/', include('multimedia.urls')),
    path('admin/', admin.site.urls),
    path('', ensure_csrf_cookie(TemplateView.as_view(template_name='index.html'))),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [
    re_path('^.*$', ensure_csrf_cookie(TemplateView.as_view(template_name='index.html'))),
]
