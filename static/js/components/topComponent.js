import { getNameDays, getWeather } from '../Api.js';

const topComponent = async () => {
  const nameDaysList = await getNameDays();
  const weather = await getWeather();
  const today = new Date();
  const date = `
    ${today.toLocaleString('pl', { weekday: 'long' })},
    ${today.getDate()}
    ${today.toLocaleString('pl', { month: 'long' })}`;
  const nameDaysToday = nameDaysList.filter(
    date => date.day == today.getDate() && date.month == today.getMonth() + 1
  )[0].names;
  const weatherAwesomeScript = document.createElement('script');
  weatherAwesomeScript.src = 'https://kit.fontawesome.com/a076d05399.js';
  document.head.appendChild(weatherAwesomeScript);
  const iconTranslate = {
    '01d': 'sun',
    '02d': 'cloud-sun',
    '03d': 'cloud',
    '04d': 'cloud-meatball',
    '09d': 'cloud-showers-heavy',
    '10d': 'cloud-sun-rain',
    '11d': 'poo-storm',
    '13d': 'snowflake',
    '50d': 'smog',
    '01n': 'moon',
    '02n': 'cloud-moon',
    '03n': 'cloud',
    '04n': 'cloud-meatball',
    '09n': 'cloud-showers-heavy',
    '10n': 'cloud-moon-rain',
    '11n': 'poo-storm',
    '13n': 'snowflake',
    '50n': 'smog'
  };
  document.getElementById('top').innerHTML = `
    <div class="datetime-namedays" style="font-weight:bold">
      <div class="datetime">${date}. </div>
      <div class="namedays">Imieniny:</div>
      <div class="namedays" style="font-weight:normal">${nameDaysToday.replaceAll(
    ' ',
    ', '
  )}</div>
    </div>
    <div class="weather">
    ${
  weather
    ? `
      <i class="fas fa-${iconTranslate[weather.weather[0].icon]}"></i>
      <div style="display:inline;margin-left:2px">${Math.round(
    weather.main.temp
  )}°c</div>
      `
    : ''
}
    </div>
  `;
};
export default topComponent;
