import { touchMenu } from '../touchScreen.js';
import { SearchInput, MobileMenu } from '../effects.js';
import { router, routesAdd } from '../router.js';

const menuComponent = () => {
  const header = document.getElementsByTagName('header')[0];
  document.getElementById('nav-main').innerHTML = `
    <li> <a href="/" data-router="menu">Aktualności</a> </li>
    <li> <a href="/kultura" data-router="menu">Kultura</a> </li>
    <li> <a href="/sport" data-router="menu">Sport</a> </li>
    <li> <a href="/ogloszenia" data-router="menu">Ogłoszenia</a> </li>
    <li> <a href="/kontakt" data-router="menu">Kontakt</a> </li>
  `;
  document.getElementById('nav-right').innerHTML = `
    <li id="search"><i class="fa fa-search" aria-hidden="true"></i><input type="text" / style="width:0px"><a href="/szukaj">Szukaj</a></li>
    <li id="login-group"></li>
  `;
  document.getElementById('nav-mobile').innerHTML = `
    <li id="mobile-search"><input type="text" /><a href="/szukaj">Szukaj</a></li>
    <li> <a href="/" data-router="menu">Aktualności</a> </li>
    <li> <a href="/kultura" data-router="menu">Kultura</a> </li>
    <li> <a href="/sport" data-router="menu">Sport</a> </li>
    <li> <a href="/ogloszenia" data-router="menu">Ogłoszenia</a> </li>
    <li> <a href="/kontakt" data-router="menu">Kontakt</a> </li>
    <li id="mobile-login-group"><a href="/login" class="login-button">Login</a></li>
    <li id="mobile-menu-last"><div id="close-mobile-menu"></div></li>
  `;

  const hamburger = document.getElementById('hamburger');
  const navButtons = document.querySelectorAll('[data-router]');
  const searchField = document.querySelector('#search input');
  const mobilesearchField = document.querySelector('#mobile-search input');

  routesAdd(navButtons);
  window.onscroll = () => {
    window.pageYOffset >= 50
      ? header.classList.add('stick')
      : header.classList.remove('stick');
  };
  document
    .getElementById('close-mobile-menu')
    .addEventListener('click', () => MobileMenu.hide());
  hamburger.addEventListener('click', () => {
    hamburger.classList.contains('openmenu')
      ? MobileMenu.hide()
      : MobileMenu.show();
  });
  searchField.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.querySelector('#search a').click();
    }
  });
  document.querySelector('#search a').addEventListener('click', event => {
    event.preventDefault();
    if (searchField.style.display != 'inline-block') {
      SearchInput.show();
    } else {
      if (searchField.value !== '') {
        router(`/szukaj/${searchField.value}`);
        searchField.value = '';
        window.scrollTo(0, 0);
      }
      SearchInput.hide();
    }
  });
  mobilesearchField.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.querySelector('#mobile-search a').click();
    }
  });
  document
    .querySelector('#mobile-search a')
    .addEventListener('click', event => {
      event.preventDefault();
      if (mobilesearchField.value !== '') {
        router(`/szukaj/${mobilesearchField.value}`);
        window.scrollTo(0, 0);
        mobilesearchField.value = '';
        MobileMenu.hide();
      }
    });
  touchMenu(header);
};

export default menuComponent;
