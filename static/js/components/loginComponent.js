import { router, routesAdd } from '../router.js';
import loginModal from '../modal/loginModal.js';
import UserApi from '../api/UserApi.js';

const loginComponent = async () => {
  const user = JSON.parse(localStorage.getItem('userinfo'));
  const render = user => {
    document.getElementById('login-group').innerHTML = `
    ${
  user
    ? `
      <a id="profile-button" class="profile-button" href='/profil'> <i class="fa fa-user"></i> <span>${
  user.full_name.split(' ')[0]
}</span> </a>
      <a id="logout-button" class="logout-button" href='/logout'> <span>Wyloguj</span> </a>
      `
    : `
      <a id="login-button" class="login-button" href='/login'> <i class="fa fa-user"></i> <span>Login</span> </a>
      `
} `;
    document.getElementById('mobile-login-group').innerHTML = `
    ${
  user
    ? `
      <a class="profile-button" style=";width:48vw;text-align:right;margin-right:8px" href='/profil'>
        <i class="fa fa-user"></i>
        <span>${user.full_name.split(' ')[0]}</span>
      </a>
      <a class="logout-button" style="width:40vw;text-align:left" href='/logout'> <span>Wyloguj</span></a>
    `
    : `
      <a class="login-button" href='/login'>Login</a>
    `
} `;
  };
  const loginButtons = () => {
    [...document.getElementsByClassName('login-button')].forEach(
      (button) => {
        button.addEventListener('click', async event => {
          event.preventDefault();
          loginModal();
        });
      }
    );
  };
  const logoutButtons = () => {
    routesAdd([...document.getElementsByClassName('profile-button')]);
    [...document.getElementsByClassName('logout-button')].forEach(button => {
      button.addEventListener('click', async event => {
        event.preventDefault();
        await UserApi.logout();
        render();
        loginButtons();
        router(window.location.pathname === '/profil' ? '/' : '');
      });
    });
  };
  render(user);
  loginButtons();
  logoutButtons();
};
export default loginComponent;
