import Api from '../Api.js';
import { routesAdd } from '../router.js';
import { mainNRight } from '../utils.js';

const rightComponent = async () => {
  const advertisementList = await Api.getList('advertisements', { limit: 40 });
  const newsList = await Api.getList('articles', { limit: 40 });
  const elements = [];
  (advertisementList['results']
    ? advertisementList.results
    : advertisementList
  ).forEach(item => {
    item.slug = '/ogloszenie/' + item.slug;
    elements.push(item);
  });
  (newsList['results'] ? newsList.results : newsList).forEach(item => {
    item.slug = '/artykul/' + item.slug;
    elements.push(item);
  });
  // elements.sort((a, b) => a.created.localeCompare(b.created));
  elements.sort((a, b) => -a.created.localeCompare(b.created));
  document.getElementById('right').innerHTML = `
  <div id="right-inner">
    <h4 style="margin-left:20px;margin-bottom:5px;color:#234567;font-size:18px">Ostatnio dodane</h4>
    ${elements
    .map(
      el => `
      <a href="${el.slug}">
        <div class="element">
          <span class="pubdate">${el.created.slice(11, 16)}</span> - ${
  el.title.length > 50 ? el.title.slice(0, 50) + '...' : el.title
}
        </div>
      </a>
    `
    )
    .join('\n')}
    </div>
  `;
  // const innerRight = document.querySelector('#right>div');
  routesAdd([...document.getElementById('right').getElementsByTagName('a')]);
  mainNRight();
};
export default rightComponent;
