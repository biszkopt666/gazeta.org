import { router } from './router.js';
import { acceptCookies, bgEvents } from './utils.js';
import topComponent from './components/topComponent.js';
import rightComponent from './components/rightComponent.js';
import loginComponent from './components/loginComponent.js';
import menuComponent from './components/menuComponent.js';

window.addEventListener('popstate', () => router());
document.addEventListener('DOMContentLoaded', async()=>{
  menuComponent();
  loginComponent();
  topComponent();
  rightComponent();
  acceptCookies();
  bgEvents();
  router();
});
