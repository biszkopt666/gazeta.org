!(function(e) {
  var t = {};
  function n(a) {
    if (t[a]) return t[a].exports;
    var i = (t[a] = { i: a, l: !1, exports: {} });
    return e[a].call(i.exports, i, i.exports, n), (i.l = !0), i.exports;
  }
  (n.m = e),
  (n.c = t),
  (n.d = function(e, t, a) {
    n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: a });
  }),
  (n.r = function(e) {
    'undefined' != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: 'Module' }),
    Object.defineProperty(e, '__esModule', { value: !0 });
  }),
  (n.t = function(e, t) {
    if ((1 & t && (e = n(e)), 8 & t)) return e;
    if (4 & t && 'object' == typeof e && e && e.__esModule) return e;
    var a = Object.create(null);
    if (
      (n.r(a),
      Object.defineProperty(a, 'default', { enumerable: !0, value: e }),
      2 & t && 'string' != typeof e)
    )
      for (var i in e)
        n.d(
          a,
          i,
          function(t) {
            return e[t];
          }.bind(null, i)
        );
    return a;
  }),
  (n.n = function(e) {
    var t =
        e && e.__esModule
          ? function() {
            return e.default;
          }
          : function() {
            return e;
          };
    return n.d(t, 'a', t), t;
  }),
  (n.o = function(e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }),
  (n.p = ''),
  n((n.s = 0));
})([
  function(e, t, n) {
    'use strict';
    n.r(t);
    const a = () => {
        const e = document.getElementById('nav-mobile'),
          t = document.getElementById('hamburger');
        if (t.classList.contains('openmenu')) {
          t.classList.remove('openmenu');
          let n = 20;
          const a = setInterval(() => {
            0 == n ? ((e.style.display = 'none'), clearInterval(a)) : n--,
            (e.style.opacity = 0.1 * n);
          }, 10);
          document.body.style.overflowY = 'auto';
        }
      },
      i = () => {
        const e = document.getElementById('nav-mobile');
        document.getElementById('hamburger').classList.add('openmenu');
        let t = 0;
        (e.style.opacity = 0), (e.style.display = 'block');
        const n = setInterval(() => {
          10 == t ? clearInterval(n) : t++, (e.style.opacity = 0.1 * t);
        }, 10);
        document.body.style.overflowY = 'hidden';
      },
      o = {
        hide: () => {
          const e = document.querySelector('#search input'),
            t = e.clienWidth > 150 ? 15 : 7;
          if ('0px' != e.style.width) {
            let n = 20;
            const a = setInterval(() => {
              0 == n
                ? ((e.style.display = ''),
                (e.style.marginLeft = '0px'),
                clearInterval(a))
                : n--,
              (e.style.width = t * n + 'px'),
              (e.style.opacity = 0.05 * n);
            }, 10);
          }
        },
        show: () => {
          const e = document.querySelector('#search input'),
            t = document.getElementById('nav-right').offsetLeft > 1030 ? 15 : 7;
          let n = 0;
          (e.style.display = 'inline-block'), (e.style.marginLeft = '2px');
          const i = setInterval(() => {
            20 == n ? (clearInterval(i), e.select()) : n++,
            (e.style.width = t * n + 'px'),
            (e.style.opacity = 0.05 * n);
          }, 10);
          document.getElementById('main').addEventListener(
            'click',
            () => {
              o.hide(), a();
            },
            { once: !0 }
          );
        }
      },
      s = () => {
        const e = document.getElementById('hidden'),
          t = document.getElementById('close');
        let n = 20;
        const a = setInterval(() => {
          0 == n
            ? ((e.style.display = 'none'),
            (t.style.display = 'none'),
            (e.innerHTML = ''),
            (document.body.style.overflowY = 'auto'),
            clearInterval(a))
            : n--,
          (e.style.opacity = 0.1 * n),
          (t.style.opacity = 0.1 * n);
        }, 10);
      },
      r = () => {
        const e = document.getElementById('hidden');
        if ('block' !== e.style.display) {
          const t = document.getElementById('close');
          let n = 0;
          (e.style.opacity = 0),
          (e.style.display = 'block'),
          (t.style.opacity = 0),
          (t.style.display = 'block'),
          (document.body.style.overflowY = 'hidden');
          const a = setInterval(() => {
            10 == n ? clearInterval(a) : n++,
            (e.style.opacity = 0.1 * n),
            (t.style.opacity = 0.1 * n);
          }, 10);
        }
      },
      l = e => {
        const t = document.getElementById('loading');
        if (e) {
          let e = 20;
          const n = setInterval(() => {
            0 == e ? ((t.style.display = 'none'), clearInterval(n)) : e--,
            (t.style.opacity = 0.1 * e);
          }, 10);
        } else t.style.display = 'none';
      },
      c = e => {
        const t = document.getElementById('loading');
        let n = 0;
        if (((t.style.display = 'block'), e)) {
          t.style.opacity = 0;
          const e = setInterval(() => {
            10 == n ? clearInterval(e) : n++, (t.style.opacity = 0.1 * n);
          }, 10);
        } else t.style.opacity = 1;
      },
      d = (e, t, n) => {
        n.style.opacity = 0;
      },
      u = (e, t, n) => {
        const a = -window.innerWidth / 2,
          i = 1.5 * window.innerWidth,
          o = window.innerWidth / 20;
        let s = 0;
        const r = setInterval(() => {
          40 == s
            ? ((n.style.left = '50%'), clearInterval(r))
            : (t < e && (n.style.left = a + (s - 20) * o + 'px'),
            t > e && (n.style.left = i - (s - 20) * o + 'px'),
            (n.style.opacity = 1),
            s++);
        }, 5);
      },
      p = () => {
        if (window.innerWidth >= 800) {
          const e = document.getElementById('right-inner'),
            t = document.getElementById('right'),
            n = document.getElementById('main');
          if (e) {
            const a = e.querySelectorAll('a');
            [...a].forEach(e => {
              e.style.display = 'unset';
            });
            let i = 1;
            for (; n.offsetHeight < t.offsetHeight; )
              ([...a][a.length - i].style.display = 'none'), i++;
          }
        }
      },
      m = e => {
        for (
          let t = 0;
          t < decodeURIComponent(document.cookie).split(';').length;
          t++
        ) {
          let n = decodeURIComponent(document.cookie).split(';')[t];
          for (; ' ' == n.charAt(0); ) n = n.substring(1);
          if (0 == n.indexOf(e + '='))
            return n.substring((e + '=').length, n.length);
        }
        return '';
      },
      y = e => {
        switch (e) {
        case 'aktualnosci':
          return 'news';
        case 'kultura':
          return 'culture';
        case 'sport':
          return 'sport';
        case 'news':
          return 'aktualnosci';
        case 'culture':
          return 'kultura';
        default:
          return e;
        }
      },
      h = e => {
        const t = new Headers();
        t.append('Content-Type', 'application/json'),
        t.append('X-Requested-With', 'XMLHttpRequest');
        try {
          t.append(
            'Authorization',
            'Token ' + JSON.parse(localStorage.getItem('userinfo')).token
          );
        } catch {
          localStorage.removeItem('userinfo');
        }
        return (
          t.append('X-CSRFToken', m('csrftoken')),
          e &&
            Object.keys(e).length > 0 &&
            Object.keys(e).forEach(n => {
              t.delete(n), (e[n] || '' !== e[n]) && t.append(n, e[n]);
            }),
          t
        );
      };
    var g = (e, t) => {
      (document.getElementById(
        'hidden'
      ).innerHTML = `\n  <form action="" method="post" id="confirm-form">\n    <h3>${e}</h3>\n    <button type="submit" class="half-width btn btn-danger">Tak</button>\n    <button type="button" class="half-width btn btn-primary">Nie</button>\n  </form>\n  `),
      r(),
      document
        .getElementById('confirm-form')
        .addEventListener('submit', e => {
          e.preventDefault(), s(), t(!0);
        }),
      document
        .querySelector('#confirm-form button[type="button"]')
        .addEventListener('click', () => {
          s(), t(!1);
        });
    };
    const f = {
      401: 'Nie masz uprawnień do oglądania tej treści',
      404: 'Strona o podanym adresie nie istnieje'
    };
    var v = {
      render: (e, t) => {
        l(),
        (main.innerHTML = `\n    <h1 class="error-page" style="text-align:center">\n      Error: ${e ||
            ''} ${
          e ? f[e] : 'Wystąpił nieznany błąd serwera'
        }\n    </h1>\n    `);
      },
      log: (e, t) => {},
      alert: (e, t) => {}
    };
    const b = {
      articles: 'api/articles',
      advertisements: 'api/adios',
      advertisementsCategories: 'api/adios/category',
      photos: 'api/photos'
    };
    var w = {
      getList: async (e, t) => {
        try {
          let n = `http://localhost:8000/${b[e]}/`;
          t &&
            Object.keys(t).length > 0 &&
            ((n += '?'),
            Object.keys(t).forEach((e, a) => {
              (n += `${e}=${t[e]}`),
              Object.is(Object.keys(t).length - 1, a) || (n += '&');
            }));
          const a = await fetch(n, { method: 'GET', headers: h() });
          if (200 === a.status) return a.json();
          v.render(a.status);
        } catch (e) {
          return v.render(!1, 'Brak połączenia z serwerem'), !1;
        }
      },
      get: async (e, t) => {
        try {
          const n = await fetch(`http://localhost:8000/${b[e]}/${t}`, {
            method: 'GET',
            headers: h()
          });
          return 200 === n.status
            ? n.json()
            : (v.render(n.status, n.statusText), null);
        } catch (e) {
          v.render(!1, 'Brak połączenia z serwerem');
        }
      },
      create: async (e, t) => {
        try {
          return (
            await fetch(`http://localhost:8000/${b[e]}/`, {
              method: 'POST',
              body: t,
              headers: h({ 'Content-Type': '' })
            })
          ).json();
        } catch {
          v.render(!1);
        }
      },
      update: async (e, t, n) => {
        try {
          const a = await fetch(`http://localhost:8000/${b[e]}/${n}`, {
            method: 'PUT',
            body: t,
            headers: h({ 'Content-Type': '' })
          });
          return a.ok || 400 === a.status ? a.json() : (v.render(a.status), !1);
        } catch {
          v.render(!1);
        }
      },
      delete: async (e, t) => {
        try {
          return await fetch(`http://localhost:8000/${b[e]}/${t}/`, {
            method: 'DELETE',
            headers: h({ 'Content-Type': '' })
          });
        } catch {
          v.render(!1);
        }
      }
    };
    const k = {
      script: () => {
        M([...document.getElementsByClassName('title')]),
        M([...document.getElementsByClassName('edit')]),
        M([...document.querySelectorAll('a.cover-mini')]),
        M([...document.querySelectorAll('#paginator a')]);
        let e = 0;
        [...document.getElementsByClassName('brick')].forEach((t, n) => {
          (t.querySelector('.image').style.height =
            t.clientHeight - 45 - t.querySelector('h4').clientHeight + 'px'),
          t.clientHeight > e && (e = t.clientHeight);
        }),
        [...document.getElementsByClassName('brick')].forEach((t, n) => {
          e != t.clientHeight &&
              (t.querySelector('.image').style.height =
                t.querySelector('.image').clientHeight +
                (e - t.clientHeight) +
                'px');
        }),
        [...document.getElementsByClassName('delete')].forEach((e, t) => {
          e.addEventListener('click', e => {
            e.preventDefault(),
            g('Czy na pewno chcesz usunąć ten artykuł?', async t => {
              if (t) {
                c();
                if (
                  '204' ==
                      (
                        await w.delete(
                          'articles',
                          e.target.pathname.split('/')[2]
                        )
                      ).status
                ) {
                  [
                    ...(await w.getList('photos', {
                      category: e.target.pathname.split('/')[2]
                    }))
                  ].forEach(e => {
                    w.delete('photos', e.id);
                  }),
                  router();
                } else l();
              }
            });
          });
        });
      },
      render: async () => {
        const e = H('strona') ? (H('strona') - 1) * n : 0,
          t = JSON.parse(localStorage.getItem('userinfo')),
          n = t ? 59 : 60,
          a = await w.getList('articles', {
            category: y(C().resource || 'aktualnosci').slice(0, 1),
            limit: n,
            offset: e
          });
        if (a) {
          Math.ceil(a.count / n);
          const e = parseInt(H('strona') ? H('strona') : 1);
          return `\n      <div id="main-inner">\n      ${
            t
              ? '\n        <article class="brick add-brick">\n          <div class="image" style="display:table">\n\n            <a href="/artykul/nowy" class="edit"><i class="fas fa-plus"></i></a>\n            <h4></h4>\n          </div>\n        </article>\n        '
              : ''
          }\n      ${(a.results ? a.results : a)
            .map(
              e =>
                `\n        <article class="brick">\n        ${
                  e.edit
                    ? `\n          <div class="buttons">\n            <a href="/artykul/${e.slug}/edytuj" class="edit btn btn-primary btn-mini">Edytuj</a>\n            <a href="/artykul/${e.slug}/usun" class="delete btn btn-danger btn-mini">Usun</a>\n          </div>\n          `
                    : ''
                }\n          <a href="/artykul/${
                  e.slug
                }" class="cover-mini">\n          ${
                  e.cover
                    ? ` <img class="image" src="${e.cover_mini || e.cover}" /> `
                    : ' <div class="image"></div> '
                }\n          </a>\n          <a class="title" href="/artykul/${
                  e.slug
                }"><h4>${
                  e.title.length > 75 ? e.title.slice(0, 75) + '...' : e.title
                }</h4></a>\n          <div class="author">Autor: ${
                  e.author
                }</div>\n        </article>\n            `
            )
            .join(
              '\n'
            )}\n        <div\n          id="paginator"\n          style="display:table;width:100%;font-size:30px;letter-spacing:-4px"\n        >\n        ${
            a.previous
              ? `\n          <a href="?strona=${e -
                  1}">\n            <div\n            style="float:left;\n            display:table"\n            >\n              <i class="fa fa-chevron-left" aria-hidden="true"></i>\n              <i class="fa fa-chevron-left" aria-hidden="true"></i>\n            </div>\n          </a>\n          `
              : ''
          }\n        ${
            a.next
              ? `\n          <a href="?strona=${e +
                  1}">\n            <div\n            style="float:right;margin-right:5px; display:table"\n            >\n              <i class="fa fa-chevron-right" aria-hidden="true"></i>\n              <i class="fa fa-chevron-right" aria-hidden="true"></i>\n            </div>\n          </a>\n          `
              : ''
          }\n        </div>\n      </div>\n    `;
        }
      }
    };
    var E = (e, t) => {
      let n = parseInt(t);
      document.getElementById('hidden').innerHTML =
        '\n    <div id="photogallery-main" > <img src="" /> </div>\n    <div id="photogallery-previmg" > <i class="fa fa-chevron-left" aria-hidden="true"></i> </div>\n    <div id="photogallery-nextimg" > <i class="fa fa-chevron-right" aria-hidden="true"></i> </div>\n  ';
      const a = document.getElementById('photogallery-previmg'),
        i = document.getElementById('photogallery-nextimg'),
        o = document.querySelector('#photogallery-main>img'),
        s = document.getElementById('photogallery-main'),
        l = t => {
          if ('' !== t) {
            d(n, t, s);
            const r = new Image();
            (r.src = e[t].original),
            r.addEventListener('load', () => {
              (e => {
                const t = e.width,
                  n = e.height;
                let a = 0,
                  i = 0;
                e.width > window.innerWidth - 40
                  ? ((a = window.innerWidth - 40),
                  (i = (window.innerWidth - 40) / (t / n)))
                  : ((a = t), (i = n)),
                i > window.innerHeight - 40 &&
                      ((a = (window.innerHeight - 40) * (t / n)),
                      (i = window.innerHeight - 40)),
                (o.style.width = a + 'px'),
                (o.style.height = i + 'px');
              })(r),
              (t => {
                t > 0
                  ? ((a.dataset.index = t - 1), (a.style.opacity = 1))
                  : ((a.dataset.index = ''), (a.style.opacity = 0)),
                t < e.length - 1
                  ? ((i.dataset.index = parseInt(t) + 1),
                  (i.style.opacity = 1))
                  : ((i.dataset.index = ''), (i.style.opacity = 0));
              })(t),
              u(n, t, s),
              (o.src = r.src),
              (n = parseInt(t));
            });
          }
        };
      [a, i].forEach(e => {
        e.addEventListener('click', () => {
          l(e.dataset.index);
        });
      }),
      ((e, t) => {
        const n = window.innerWidth / 2;
        let a = 0,
          i = 0;
        e.addEventListener('touchstart', function(e) {
          const t = e.changedTouches[0];
          (i = 0), (a = parseInt(t.clientX)), e.preventDefault();
        }),
        e.addEventListener('touchmove', function(t) {
          const o = t.changedTouches[0];
          (i = parseInt(o.clientX) - a),
          (e.style.left = n + i + 'px'),
          t.preventDefault();
        }),
        e.addEventListener('touchend', function() {
          Math.abs(i) > window.innerWidth / 2
            ? t(i > 0 ? 'prev' : 'next')
            : (e.style.left = '50%');
        });
      })(s, async t => {
        t &&
            ('next' === t && n < e.length - 1
              ? l(n + 1)
              : 'prev' === t && n > 0
                ? l(n - 1)
                : (s.style.left = '50%'));
      }),
      (document.onkeydown = e => {
        switch (e.keyCode) {
        case 37:
          a.click();
          break;
        case 39:
          i.click();
        }
      }),
      l(t),
      r();
    };
    const $ = {
      script: e => {
        const t = JSON.parse(sessionStorage.getItem('photos'));
        sessionStorage.removeItem('photos');
        const n = document.getElementById('article-thumbnails'),
          a = document.getElementById('add-photos'),
          i = document.getElementById('photos-input-confirm'),
          o = document.getElementById('photos-confirm-alert');
        (document.title =
          'Gazeta.org - ' + document.getElementById('title').innerHTML),
        M([document.querySelector('#go-back a')]),
        document.querySelector('.article .buttons') &&
            (M([document.querySelector('.article .edit')]),
            document
              .querySelector('.article .delete')
              .addEventListener('click', t => {
                event.preventDefault(),
                g('Czy na pewno chcesz usunąć ten artykuł?', async t => {
                  if (t) {
                    c();
                    if (
                      204 ===
                        (
                          await w.delete(
                            'articles',
                            event.target.pathname.split('/')[2]
                          )
                        ).status
                    ) {
                      [
                        ...(await w.getList('photos', { category: e }))
                      ].forEach(e => {
                        w.delete('photos', e.id);
                      }),
                      N('/' + event.target.dataset.category);
                    } else l();
                  }
                });
              })),
        [...document.querySelectorAll('a.photo-link')].forEach((e, n) => {
          e.addEventListener('click', n => {
            n.preventDefault(), E(t, e.dataset.index);
          });
        });
        const s = e => {
          g('Czy na pewno chcesz usunąć to zdjęcie?', async t => {
            if (t) {
              c();
              204 === (await w.delete('photos', e.dataset.id)).status &&
                (e.parentElement.remove(), l());
            }
          });
        };
        a &&
          (a.addEventListener('change', () => {
            const e = [...a.files],
              s = () => {
                e.length > 0
                  ? ((i.style.display = 'block'),
                  (i.style.opacity = 1),
                  (o.style.opacity = 1))
                  : ((i.style.display = 'none'),
                  (i.style.opacity = 0),
                  (o.style.opacity = 0));
              };
            [...document.getElementsByClassName('new-photo')].forEach(e => {
              e.remove();
            }),
            e.forEach((a, i) => {
              const o = URL.createObjectURL(a),
                r = document.createElement('DIV');
              r.classList.add('flagstone', 'new-photo'),
              (r.innerHTML = `\n          <a href="" style="" data-action="${t.length +
                    i}" data-i="${i}">\n            <div class="photos-action" style="">\n              <i class="fa fa-times" aria-hidden="true" style="color: #dc0000"></i>\n            </div>\n          </a>\n          <a href="" data-index="${t.length +
                    i}">\n            <img src="${o}" class="thumbnail" />\n          </a>\n          `),
              r.addEventListener('click', e => {
                e.preventDefault();
              });
              const l = r.getElementsByTagName('a')[0];
              l.addEventListener(
                'click',
                t => {
                  t.preventDefault(),
                  e.splice(l.dataset.i, 1),
                  [...document.querySelectorAll('[data-i]')].forEach(e => {
                    e.dataset.i > l.dataset.i &&
                          ((e.dataset.i = e.dataset.i - 1),
                          (e.dataset.index = e.dataset.index - 1));
                  }),
                  l.parentElement.remove(),
                  s();
                },
                { once: !0 }
              ),
              n.appendChild(r);
            }),
            s();
          }),
          i.addEventListener('click', () => {
            const n = t.length;
            [...a.files].forEach(async (a, i) => {
              let o = new FormData();
              o.append('category', e), o.append('original', a);
              const r = await w.create('photos', o);
              if (r.mini) {
                const e = document.querySelector(`[data-index="${n + i}"]`),
                  a = document.querySelector(`[data-action="${n + i}"]`);
                (e.href = r.original),
                (e.innerHTML = `<img src="${r.mini}" class="thumbnail">`),
                e.parentElement.classList.remove('new-photo'),
                t.push({
                  id: r.id,
                  category: r.category,
                  original: r.original,
                  mini: r.mini
                }),
                e.addEventListener('click', n => {
                  n.preventDefault(), E(t, e.dataset.index);
                }),
                a.addEventListener('click', e => {
                  e.preventDefault(), s(a);
                });
              }
            }),
            (i.style.display = 'none'),
            (i.style.opacity = 0),
            (o.style.opacity = 0);
          })),
        [...document.getElementsByClassName('photo-remove')].forEach(e => {
          e.addEventListener('click', t => {
            t.preventDefault(), s(e);
          });
        });
      },
      render: async e => {
        const t = await w.get('articles', e),
          n = await w.getList('photos', { category: e });
        sessionStorage.setItem('photos', JSON.stringify(n));
        if (t)
          return `\n      <div>\n        <div id="go-back">\n          <a href="/${y(
            t.category
          )}" >\n            <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>\n          </a>\n        </div>\n        <div class="article">\n        ${
            t.edit
              ? `\n          <div class="buttons">\n            <a href="/artykul/${
                t.slug
              }/edytuj" class="edit btn btn-primary btn-mini">Edytuj</a>\n            <a href="/artykul/${
                t.slug
              }/usun" class="delete btn btn-danger btn-mini" data-category="${y(
                t.category
              )}">Usun</a>\n          </div>\n        `
              : ''
          }\n        ${
            t
              ? `\n          <h3 id="title">${t.title}</h3>${
                t.cover ? `<img src="${t.cover}" />` : ''
              }\n          <div class="main-content">${(e => {
                let t = '';
                return (
                  e.split('\n').forEach(e => {
                    e.length > 1 && (t += `<p>${e}</p>`);
                  }),
                  t
                );
              })(t.content)}</div>\n        `
              : '<h3 id="title">Artykuł nie istnieje</h3>' +
                window.history.back()
          }\n          <div class="photo-thumbnails" id="article-thumbnails" >\n          ${
            t.edit
              ? '\n          <div id="photos-confirm-alert" style="width:100%;text-align:center;font-size:20px;font-weight:bold;color:#da0000;opacity:0">Potwierdź aby przesłać</div>\n            <div class="flagstone">\n                <div class="photos-action" style="opacity:0;display:none;cursor:pointer" id="photos-input-confirm">\n                  <i class="fa fa-check" aria-hidden="true" style="color: #00ee00;"></i>\n                </div>\n              <label for="add-photos" class="image-upload thumbnail" style="padding:0px;text-align:center" >\n                <i class="fas fa-upload" style="font-size:40px;margin-top:20px;"></i>\n              </label>\n              <input type="file" name="add-photos" id="add-photos" multiple />\n            </div>\n            '
              : ''
          }\n            ${n
            .map(
              (e, n) =>
                `\n            <div class="flagstone">\n            ${
                  t.edit
                    ? `\n              <a href="/zdjecie/${e.id}/usun" class="photo-remove" data-id="${e.id}">\n                <div class="photos-action">\n                  <i class="fa fa-times" aria-hidden="true" style="color: #dc0000"></i>\n                </div>\n              </a>\n              `
                    : ''
                }\n              <a href="${
                  e.original
                }" class="photo-link" data-index="${n}"> <img src="${
                  e.mini
                }" class="thumbnail" ></a>\n            </div>\n            `
            )
            .join(
              '\n'
            )}\n\n          </div>\n        </div>\n      </div>\n      `;
      }
    };
    var L = async () => {
      const e = await w.getList('advertisements', { limit: 40 }),
        t = await w.getList('articles', { limit: 40 }),
        n = [];
      (e.results ? e.results : e).forEach(e => {
        (e.slug = '/ogloszenie/' + e.slug), n.push(e);
      }),
      (t.results ? t.results : t).forEach(e => {
        (e.slug = '/artykul/' + e.slug), n.push(e);
      }),
      n.sort((e, t) => -e.created.localeCompare(t.created)),
      (document.getElementById(
        'right'
      ).innerHTML = `\n  <div id="right-inner">\n    <h4 style="margin-left:20px;margin-bottom:5px;color:#234567;font-size:18px">Ostatnio dodane</h4>\n    ${n
        .map(
          e =>
            `\n      <a href="${
              e.slug
            }">\n        <div class="element">\n          <span class="pubdate">${e.created.slice(
              11,
              16
            )}</span> - ${
              e.title.length > 50 ? e.title.slice(0, 50) + '...' : e.title
            }\n        </div>\n      </a>\n    `
        )
        .join('\n')}\n    </div>\n  `),
      M([...document.getElementById('right').getElementsByTagName('a')]),
      p();
    };
    const I = {
      script: e => {
        M([...document.querySelectorAll('#main a')]);
      },
      render: async e => {
        const t = await w.getList('articles', { search: e || '' }),
          n = e.replace('%20', ' ');
        return `\n    <div>\n      <h3>Wyniki wyszukiwania dla "${n}"</h3>\n      ${(t.results
          ? t.results
          : t
        )
          .map(
            e =>
              `\n          <article>\n          <a href='/artykul/${
                e.slug
              }'><h3>${(e => {
                const t = e.toLowerCase().indexOf(n);
                return t > 0
                  ? `${e.slice(0, t)}<span class="search-fragment">${e.slice(
                    t,
                    t + n.length
                  )}</span>${e.slice(t + n.length, e.length)}`
                  : e;
              })(e.title)}</h3></a>\n          <p>${(e => {
                const t = e.toLowerCase().indexOf(n);
                let a = 0,
                  i = e.length;
                return (
                  t > 100 && (a = t - 100),
                  e.length - t - n.length > 100 && (i = t + n.length + 100),
                  t >= 0
                    ? `...${e.slice(
                      a,
                      t
                    )}<span class="search-fragment">${e.slice(
                      t,
                      t + n.length
                    )}</span>${e.slice(t + n.length, i)}...`
                    : e.length > 200
                      ? `...${e.slice(
                        parseInt(e.length / 2 - 100),
                        parseInt(e.length / 2 + 100)
                      )}...`
                      : e
                );
              })(e.content)}<p>\n          </article>\n          `
          )
          .join('\n')}\n    </div>\n    `;
      }
    };
    const S = {
      script: async e => {
        const t = JSON.parse(localStorage.getItem('userinfo')),
          n = () => {
            M([document.querySelector('#ad-detail .edit')]),
            document
              .querySelector('#ad-detail .delete')
              .addEventListener('click', e => {
                event.preventDefault(),
                g('Czy na pewno chcesz to ogłoszenie?', async e => {
                  if (e) {
                    Loading.show();
                    204 ===
                          (
                            await w.delete(
                              'advertisements',
                              event.target.pathname.split('/')[2]
                            )
                          ).status &&
                          (document.getElementById('ad-detail').innerHTML = ''),
                    Loading.hide();
                  }
                });
              });
          };
        M([...document.querySelectorAll('#paginator a')]),
        t && M([document.getElementById('ad-add')]),
        document.querySelectorAll('#ad-table a').forEach(e => {
          e.addEventListener('click', async a => {
            a.preventDefault();
            const i = await S.renderDetail(e.pathname.split('/')[2], t);
            (document.getElementById('ad-detail').innerHTML = i),
            t && n(),
            history.pushState(
              null,
              null,
              e.pathname + window.location.search
            ),
            window.scrollTo(0, 50),
            p();
          });
        }),
        t && e && n(),
        (document.querySelector('.ad-form select').selectedIndex = 0),
        document.querySelectorAll('#ad-fields>*').forEach(e => {
          e.addEventListener('keyup', function(e) {
            13 === e.keyCode &&
                (e.preventDefault(),
                document.getElementById('ad-search').click());
          });
        }),
        document
          .getElementById('ad-search')
          .addEventListener('click', async () => {
            const e = new Object();
            document.querySelectorAll('#ad-fields>*').forEach(t => {
              t.value && (e[t.name] = t.value);
            });
            let t = '';
            e &&
                Object.keys(e).length > 0 &&
                ((t += '?'),
                Object.keys(e).forEach((n, a) => {
                  (t += `${n}=${e[n]}`),
                  Object.is(Object.keys(e).length - 1, a) || (t += '&');
                })),
            N('/ogloszenia/' + t);
          });
      },
      renderDetail: async (e, t) => {
        const n = await w.get('advertisements', e);
        return n
          ? `\n      ${
            t
              ? `\n        <div class="buttons">\n          <a href="/ogloszenie/${n.slug}/edytuj" class="edit btn btn-primary btn-mini">Edytuj</a>\n          <a href="/ogloszenie/${n.slug}/usun" class="delete btn btn-danger btn-mini" data-category="${n.category}">Usun</a>\n        </div>\n      `
              : ''
          }\n      <div class="underline-box">\n        <h3>${
            n.title
          }</h3>\n        <div class="main-content">\n          <p>${(e => {
            let t = '';
            return (
              e.split('\n').forEach(e => {
                e.length > 1 && (t += `<p>${e}</p>`);
              }),
              t
            );
          })(
            n.content
          )}</p>\n        </div>\n        <div class="price-contact">\n          <span>${
            n.category_name
          }</span>\n          <span style="float:right">Cena: ${String(
            n.price
          ).replace(
            /(\d)(?=(\d{3})+$)/g,
            '$1 '
          )} zł.</span>\n        </div>\n      </div>\n      `
          : '';
      },
      render: async e => {
        const t = H();
        (t.limit = 40), (t.offset = H('strona') ? 40 * (H('strona') - 1) : 0);
        const n = JSON.parse(localStorage.getItem('userinfo')),
          a = await w.getList('advertisements', t),
          i = !!e && (await S.renderDetail(e, n)),
          o = await w.getList('advertisementsCategories');
        if (a && o) {
          const t = parseInt(H('strona') ? H('strona') : 1),
            s = e => {
              const t = window.location.search;
              return t
                ? t.includes('strona')
                  ? t.replace('strona=' + H('strona'), 'strona=' + e)
                  : `${t}&strona=${e}`
                : '?strona=' + e;
            };
          return `\n      <div style="over">\n        <div class="ad-form underline-box">\n          <div id="ad-fields">\n            <input type="text" class="inputborder" name="item" placeholder="Czego szukasz?" />\n            <select name="category" class="inputborder" >\n              <option value="">Wszystko</option>\n              ${o
            .map(
              e =>
                `\n              <option value="${e.slug}">${e.name}</option>\n                `
            )
            .join(
              '\n'
            )}\n              </select>\n            <input type="number" class="inputborder" name="price" placeholder="cena max." />\n          </div>\n          <div><input type="button" id="ad-search" class="btn btn-primary" value="Szukaj" ></input></div>\n        </div>\n        ${
            n
              ? '\n        <div>\n          <a href="/ogloszenie/nowy" id="ad-add" style="color:white">\n            <div class="btn btn-primary" style="text-align:center;margin-bottom:5px;">Dodaj</div>\n          </a>\n        </div>\n        '
              : ''
          }\n        <div id="ad-detail">${
            e ? i : ''
          }</div>\n        <div class="table adv-table" id="ad-table">\n        ${
            a.count
              ? `\n          ${(a.results ? a.results : a)
                .map(
                  e =>
                    `\n          <div class="row">\n            <div><a href="/ogloszenie/${
                      e.slug
                    }">${
                      e.title
                    }</a></div>\n            <div class="cell-price">${String(
                      e.price
                    ).replace(
                      /(\d)(?=(\d{3})+$)/g,
                      '$1 '
                    )} zł.</div>\n          </div> `
                )
                .join('\n')}\n          `
              : '\n          <h3>Brak ogłoszeń spełniających kryteria</h3>\n          '
          }\n        </div>\n        <div\n          id="paginator"\n          style="display:table;width:100%;font-size:30px;letter-spacing:-4px"\n        >\n        ${
            a.previous
              ? `\n          <a href="/ogloszenia${s(
                t - 1
              )}">\n            <div\n            style="float:left;\n            display:table"\n            >\n              <i class="fa fa-chevron-left" aria-hidden="true"></i>\n              <i class="fa fa-chevron-left" aria-hidden="true"></i>\n            </div>\n          </a>\n          `
              : ''
          }\n        ${
            a.next
              ? `\n          <a href="/ogloszenia${s(
                t + 1
              )}">\n            <div\n            style="float:right;margin-right:5px; display:table"\n            >\n              <i class="fa fa-chevron-right" aria-hidden="true"></i>\n              <i class="fa fa-chevron-right" aria-hidden="true"></i>\n            </div>\n          </a>\n          `
              : ''
          }\n        </div>\n      </div>\n      `;
        }
      }
    };
    var x = (e, t) => {
      (document.getElementById(
        'hidden'
      ).innerHTML = `\n  <form action="" method="post" id="form-inputtext">\n    <h3>${e}</h3>\n    <input type="text"  class="inputborder" name="title" />\n    <button type="submit" class="half-width btn btn-danger">OK</button>\n    <button type="button" class="half-width btn btn-primary">Anuluj</button>\n  </form>\n  `),
      r();
      const n = document.querySelector('#form-inputtext input');
      document
        .getElementById('form-inputtext')
        .addEventListener('submit', e => {
          e.preventDefault(),
          n.value ? (t(n.value), s()) : n.classList.add('warning');
        }),
      document
        .querySelector('#form-inputtext button[type="button"]')
        .addEventListener('click', () => {
          s(), t(!1);
        });
    };
    var j = {
      login: async (e, t) => {
        try {
          const n = await fetch('http://localhost:8000/api/accounts/login/', {
            method: 'POST',
            body: JSON.stringify({ email: e, password: t }),
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            }
          });
          return (
            console.log('Login Status', n.status),
            200 === n.status || 400 === n.status ? n.json() : null
          );
        } catch {
          v.render(!1);
        }
      },
      logout: async e => {
        try {
          e &&
            (await fetch('http://localhost:8000/api/accounts/logout/', {
              method: 'POST',
              headers: h()
            })),
          localStorage.removeItem('userinfo');
        } catch {
          v.render(!1);
        }
      },
      register: async e => {
        try {
          const t = await fetch(
            'http://localhost:8000/api/accounts/register/',
            {
              method: 'POST',
              body: JSON.stringify(e),
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              }
            }
          );
          if (200 === t.status) return t.json();
          if (400 === t.status) return t.json();
          v.render(!1);
        } catch {
          v.render(500);
        }
      },
      activate: async e => {
        try {
          const t = await fetch(
            `http://localhost:8000/api/accounts/activate/${e}/`,
            {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              }
            }
          );
          if (200 === t.status) return t.json();
          if (204 === t.status) return null;
          v.render(t.status);
        } catch {
          v.render(!1);
        }
      },
      changePassword: async e => {
        try {
          const t = await fetch(
            'http://localhost:8000/api/accounts/changepassword/',
            { method: 'PUT', body: JSON.stringify(e), headers: h() }
          );
          if (200 === t.status) return t.json();
          if (400 === t.status) return t.json();
          v.render(!1);
        } catch {
          v.render(!1);
        }
      },
      info: async e => {
        try {
          const t = await fetch(
            'http://localhost:8000/api/accounts/userinfo/',
            { method: 'GET', headers: h({ Authorization: 'Token ' + e }) }
          );
          if (200 === t.status) {
            return await t.json();
          }
          v.render(t.status, 'Sprawdź czy użytkownik został aktywowany');
        } catch (e) {
          v.render(!1);
        }
      },
      list: async e => {
        try {
          const t = await fetch(
            'http://localhost:8000/api/accounts/userlist/',
            { method: 'GET', headers: h({ Authorization: 'Token ' + e }) }
          );
          return 200 === t.status ? t.json() : null;
        } catch (e) {
          v.render(!1);
        }
      },
      delete: async e => {
        try {
          const t = await fetch(
            `http://localhost:8000/api/accounts/delete/${e}/`,
            { method: 'DELETE', headers: h() }
          );
          if (200 === t.status) return t.json();
          if (204 === t.status) return null;
          v.render(t.status);
        } catch {
          v.render(!1);
        }
      }
    };
    var z = () => {
      (document.getElementById('hidden').innerHTML =
        '\n    <form action="/changepassword" method="post" id="changepassword-form">\n      <h3>Zmień Hasło</h3>\n      <p id="errorfield"></p>\n      <input type="password" class="inputborder" placeholder="stare hasło" name="old" />\n      <input type="password" class="inputborder" placeholder="nowe hasło" name="new" />\n      <input type="password" class="inputborder" placeholder="powtórz nowe hasło" name="confirm" />\n      <button type="submit" class="btn btn-primary">Zmień</button>\n    </form>\n  '),
      r();
      const e = document.getElementById('changepassword-form'),
        t = e.getElementsByTagName('input');
      e.addEventListener('submit', async e => {
        e.preventDefault(),
        [...t].forEach(e => {
          e.addEventListener('click', () => {
            e.classList.remove('warning');
          });
        });
        const n = await j.changePassword(
          Object.values(t).reduce((e, t) => ((e[t.name] = t.value), e), {})
        );
        n.success
          ? s()
          : (Object.keys(n).forEach(e => {
            t[e] && t[e].classList.add('warning');
          }),
          (document.getElementById('errorfield').innerHTML =
              'Popraw poniższe błędy'));
      });
    };
    var B = () => {
      (document.getElementById('hidden').innerHTML =
        '\n    <form action="/register" method="post" id="register-form">\n      <h3>Rejestracja</h3>\n      <p id="errorfield"></p>\n      <input type="text" class="inputborder" placeholder="imię i nazwisko" name="full_name" />\n      <input type="text" class="inputborder" placeholder="email" name="email" />\n      <input type="password" class="inputborder" placeholder="hasło" name="password" />\n      <input type="password" class="inputborder" placeholder="powtórz hasło" name="password2" />\n      <button type="submit" class="btn btn-primary">Zarejestruj</button>\n      <p class="small">Posiadasz już konto? <a href="/login">Przejdź do logowania</a></p>\n    </form>\n  '),
      r();
      const e = document.getElementById('register-form'),
        t = e.getElementsByTagName('input');
      e.addEventListener('submit', async e => {
        e.preventDefault(),
        [...t].forEach(e => {
          e.addEventListener('click', () => {
            e.classList.remove('warning');
          });
        });
        const n = await j.register(
          Object.values(t).reduce((e, t) => ((e[t.name] = t.value), e), {})
        );
        n.token
          ? s()
          : (Object.keys(n).forEach(e => {
            t[e] && t[e].classList.add('warning');
          }),
          (document.getElementById('errorfield').innerHTML =
              'Popraw poniższe błędy'));
      }),
      e.querySelector('a').addEventListener('click', e => {
        e.preventDefault(), O();
      });
    };
    var T = e => {
      (document.getElementById(
        'hidden'
      ).innerHTML = `\n  <form action="" method="post" id="confirm-form">\n    <h3>${e}</h3>\n    <button type="submit" class="btn btn-danger">OK</button>\n  </form>\n  `),
      r(),
      document
        .getElementById('confirm-form')
        .addEventListener('submit', e => {
          e.preventDefault(), s();
        });
    };
    var O = () => {
      (document.getElementById('hidden').innerHTML =
        '\n    <form action="/login" method="post" id="login-form">\n      <h3>Logowanie</h3>\n      <p id="errorfield"></p>\n      <input type="text"  class="inputborder" placeholder="email" name="email" />\n      <input type="password" class="inputborder" placeholder="hasło" name="password" />\n      <button type="submit" class="btn btn-primary">Login</button>\n      <p class="small">Nie posiadasz jeszcze konta? <a href="/rejestracja">Kliknij Tutaj!!!</a></p>\n    </form>\n  '),
      r();
      const e = document.getElementById('login-form'),
        t = e.getElementsByTagName('input');
      e.addEventListener('submit', async e => {
        e.preventDefault(),
        [...t].forEach(e => {
          e.addEventListener('click', () => {
            e.classList.remove('warning');
          });
        });
        const n = await j.login(t.email.value, t.password.value);
        if (n.token) {
          if (n.active) {
            const e = await j.info(n.token);
            localStorage.setItem(
              'userinfo',
              JSON.stringify({
                full_name: e.full_name,
                email: e.email,
                token: e.token
              })
            ),
            s(),
            N();
          } else T('Użytkownik jest nieaktywny');
          D();
        } else
          Object.keys(n).forEach(e => {
            t[e] && t[e].classList.add('warning');
          }),
          (document.getElementById('errorfield').innerHTML =
              'Popraw poniższe błędy');
      }),
      e.querySelector('a').addEventListener('click', e => {
        e.preventDefault(), B();
      });
    };
    var D = async () => {
      const e = JSON.parse(localStorage.getItem('userinfo')),
        t = e => {
          (document.getElementById('login-group').innerHTML = `\n    ${
            e
              ? `\n      <a id="profile-button" class="profile-button" href='/profil'> <i class="fa fa-user"></i> <span>${
                e.full_name.split(' ')[0]
              }</span> </a>\n      <a id="logout-button" class="logout-button" href='/logout'> <span>Wyloguj</span> </a>\n      `
              : '\n      <a id="login-button" class="login-button" href=\'/login\'> <i class="fa fa-user"></i> <span>Login</span> </a>\n      '
          } `),
          (document.getElementById('mobile-login-group').innerHTML = `\n    ${
            e
              ? `\n      <a class="profile-button" style=";width:48vw;text-align:right;margin-right:8px" href='/profil'>\n        <i class="fa fa-user"></i>\n        <span>${
                e.full_name.split(' ')[0]
              }</span>\n      </a>\n      <a class="logout-button" style="width:40vw;text-align:left" href='/logout'> <span>Wyloguj</span></a>\n    `
              : '\n      <a class="login-button" href=\'/login\'>Login</a>\n    '
          } `);
        },
        n = () => {
          [...document.getElementsByClassName('login-button')].forEach(e => {
            e.addEventListener('click', async e => {
              e.preventDefault(), O();
            });
          });
        };
      t(e),
      n(),
      M([...document.getElementsByClassName('profile-button')]),
      [...document.getElementsByClassName('logout-button')].forEach(e => {
        e.addEventListener('click', async e => {
          e.preventDefault(),
          await j.logout(),
          t(),
          n(),
          N('/profil' === window.location.pathname ? '/' : '');
        });
      });
    };
    const q = {
        '/': k,
        '/kultura': k,
        '/sport': k,
        '/art/:slug': $,
        '/artykul/:slug/edytuj': {
          script: async e => {
            M([document.querySelector('#go-back a')]);
            const t = document.getElementById('article-form');
            if (t) {
              const n = t.querySelector('select');
              n.dataset.selected && (n.value = n.dataset.selected.slice(0, 1));
              const a = t.querySelector('input[name="cover"]');
              a.addEventListener('change', () => {
                const e =
                  a.files[0].name.length > 18
                    ? `${a.files[0].name.slice(
                      0,
                      10
                    )}. .${a.files[0].name.slice(
                      a.files[0].name.length - 5,
                      a.files[0].name.length
                    )}`
                    : a.files[0].name;
                t.querySelector('label[for="cover"]').innerText = e;
              }),
              t.addEventListener('submit', async n => {
                n.preventDefault();
                let i = new FormData();
                i.append(
                  'title',
                  t.querySelector('input[name="title"]').value
                ),
                i.append('content', t.querySelector('textarea').value),
                i.append('category', t.querySelector('select').value),
                a.files[0] && i.append('cover', a.files[0]),
                c();
                const o =
                    'nowy' == e
                      ? await w.create('articles', i)
                      : await w.update('articles', i, e);
                o && o.slug
                  ? (N('/artykul/' + o.slug), L())
                  : Object.keys(o).forEach((e, t) => {
                    document
                      .getElementsByName(e)[0]
                      .classList.add('warning'),
                    l();
                  });
              });
            }
          },
          render: async e => {
            const t =
              'nowy' !== e && e
                ? await w.get('articles', e)
                : { title: '', content: '', edit: !0 };
            if (t.edit && JSON.parse(localStorage.getItem('userinfo')))
              return (
                (document.title =
                  'Gazeta.org - ' + (t.title ? 'Edytuj: ' + t.title : 'Nowy')),
                `\n      <div id="go-back">\n      <a href="/${
                  t.slug ? 'artykul/' + t.slug : ''
                }" >\n      <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>\n      </a>\n      </div>\n        <form action="/article/new" method="post" class="embed-form" id="article-form">\n          <input type="text" class="inputborder" name="title" value="${
                  t ? '' + t.title : ''
                }" />\n          <textarea name="content" class="inputborder" >${
                  t ? '' + t.content : ''
                }</textarea>\n          <label for="cover" class="image-upload"> <i class="fas fa-upload"></i> Dodaj okładkę</label>\n          <input type="file" name="cover" id="cover" />\n          <div style="float: right; margin-right: -21px;" id="form-buttons">\n            <select ${
                  t.category ? `data-selected="${t.category}"` : ''
                }>\n              <option value="n" selected >Aktualności</option>\n              <option value="c">Kultura</option>\n              <option value="s">Sport</option>\n            </select>\n            <button type="submit" class="btn btn-primary">Gotowe</button>\n          </div>\n        </form>\n      `
              );
            Error404Screen.render(403);
          }
        },
        '/artykul/:slug': $,
        '/szukaj': I,
        '/szukaj/:slug': I,
        '/ogloszenia': S,
        '/ogloszenie/:slug': S,
        '/ogloszenie/:slug/edytuj': {
          script: async e => {
            M([document.querySelector('#go-back a')]);
            const t = document.getElementById('advertisement-form');
            if (t) {
              const n = t.querySelector('select');
              n.addEventListener('change', e => {
                e.preventDefault(),
                'add' === n.value &&
                    x('Wpisz nazwę kategorii', async e => {
                      if (e) {
                        let t = new FormData();
                        t.append('name', e);
                        const a = await w.create('advertisementsCategories', t);
                        if (a.slug) {
                          const e = document.createElement('option');
                          (e.value = a.slug),
                          (e.innerHTML = a.name),
                          n.prepend(e),
                          (n.selectedIndex = 0);
                        }
                      }
                    });
              }),
              t.addEventListener('submit', async n => {
                n.preventDefault();
                let a = new FormData();
                a.append(
                  'title',
                  t.querySelector('input[name="title"]').value
                ),
                a.append('content', t.querySelector('textarea').value),
                a.append('category', t.querySelector('select').value),
                a.append(
                  'price',
                  t.querySelector('input[name="price"]').value
                ),
                c();
                const i =
                    'nowy' != e && e
                      ? await w.update('advertisements', a, e)
                      : await w.create('advertisements', a);
                i.slug
                  ? (N('/ogloszenie/' + i.slug), L())
                  : Object.keys(i).forEach((e, t) => {
                    document
                      .getElementsByName(e)[0]
                      .classList.add('warning'),
                    l();
                  });
              });
            }
          },
          render: async e => {
            const t =
                'nowy' !== e && e
                  ? await w.get('advertisements', e)
                  : { title: '', content: '', edit: !0 },
              n = await w.getList('advertisementsCategories');
            if (JSON.parse(localStorage.getItem('userinfo')))
              return `\n      <div id="go-back">\n      <a href="/${
                t.slug ? '/ogloszenie/' + t.slug : ''
              }" >\n      <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>\n      </a>\n      </div>\n        <form action="/advertisement/new" method="post" class="embed-form" id="advertisement-form">\n          <input type="text" class="inputborder" name="title" value="${
                t ? '' + t.title : ''
              }" />\n          <textarea name="content" class="inputborder" >${
                t ? '' + t.content : ''
              }</textarea>\n          <select name="category">\n                ${n
                .map(
                  e =>
                    `\n            <option value="${e.slug}">${e.name}</option>\n                  `
                )
                .join(
                  '\n'
                )}\n            <option value="add" id="add-category">+ Dodaj</option>\n          </select>\n          <input type="number" class="inputborder" name="price" placeholder="cena" value="${
                t ? '' + t.price : ''
              }" />\n          <button type="submit" class="btn btn-primary adv-submit">Gotowe</button>\n        </form>\n      `;
            Error404Screen.render(403);
          }
        },
        '/kontakt': {
          render: async () =>
            '\n      <div class="col-sm-1 col-lg-2">\n        <iframe\n          style="width:100%;height:400px"\n          frameborder="0"\n          scrolling="no"\n          marginheight="0"\n          marginwidth="0"\n          src="https://www.openstreetmap.org/export/embed.html?bbox=106.91067069768907%2C47.923514192910204%2C106.91421121358873%2C47.92508869784562&amp;layer=mapnik&amp;marker=47.924301451368706%2C106.91244095563889"\n          style="border: 1px solid black: width:200px"\n        ></iframe>\n        <br/>\n\n      </div>\n      <div class="col-sm-1 col-lg-2 address" style="text-align:center">\n        <h3>Kontakt</h3>\n        <p>17, Baga Street, Gandan, Ułan Bator, </p>\n        <p>Sukhbaatar Duureg, 210646, Mongolia</p>\n        <p>Email: <a href="mailto:czyngis@yuan.mn">mailto:czyngis@yuan.mn</a></p>\n      </div>\n    '
        },
        '/profil': {
          script: () => {
            [...document.getElementsByClassName('activate')].forEach(e => {
              e.addEventListener('click', async t => {
                t.preventDefault();
                const n = await j.activate(e.dataset.token);
                n.token &&
                  ((document.getElementById(
                    `user-${n.token}-delete`
                  ).style.display = 'block'),
                  (e.style.display = 'none'));
              });
            }),
            [...document.getElementsByClassName('delete-account')].forEach(
              e => {
                e.addEventListener('click', t => {
                  t.preventDefault(),
                  (e => {
                    g(
                      `Czy na pewno chcesz usunąć konto ${e.dataset.email}?`,
                      async t => {
                        if (t) {
                          const t = await j.delete(e.dataset.token);
                          t.token &&
                                ((document.getElementById(
                                  `user-${t.token}-activate`
                                ).style.display = 'block'),
                                (e.style.display = 'none'));
                        }
                      }
                    );
                  })(e);
                });
              }
            ),
            document
              .getElementById('change-password')
              .addEventListener('click', e => {
                e.preventDefault(), z();
              }),
            document
              .getElementById('hard-logout')
              .addEventListener('click', e => {
                e.preventDefault(),
                g(
                  'Czy na pewno chcesz się wylogować ze wszystkich urządzeń?',
                  async e => {
                    if (e) {
                      (await j.logout(!0)) && N('/');
                    }
                  }
                );
              });
          },
          render: async () => {
            const e = await j.info(
                localStorage.getItem('userinfo')
                  ? JSON.parse(localStorage.getItem('userinfo')).token
                  : ''
              ),
              t = e.is_staff
                ? await j.list(
                  localStorage.getItem('userinfo')
                    ? JSON.parse(localStorage.getItem('userinfo')).token
                    : ''
                )
                : null;
            return `\n    <div>\n    <h3>Profil użytkownika ${
              e.full_name
            }</h3>\n      <div class="buttons">\n        <a href="/haslo/zmien" class="change-password btn btn-primary" id="change-password">Zmień Hasło</a>\n        <a href="/logout/hard" id="hard-logout" class="hard-logout btn btn-primary">Wyloguj ze wszystkiego</a>\n        <a href="/konto/usun" class="delete-account btn btn-danger" data-token="${
              e.token
            }" data-email="${e.email}">Usuń Konto</a> </div>\n      ${
              t
                ? `\n        <div class="table users-table" style='margin-top:40px'>\n        ${t
                  .map(
                    e =>
                      `\n          ${
                        e.is_staff
                          ? ''
                          : `\n          <div class="row">\n            <div>${
                            e.full_name
                          }</div>\n            <div>${
                            e.email
                          }</div>\n            <div style="width:40px">\n              <a\n                id="user-${
                            e.token
                          }-delete"\n                href="/profil/${
                            e.token
                          }/usun"\n                class="delete-account btn btn-mini btn-danger"\n                style="padding:1px 15px;display:${
                            e.is_active ? 'block' : 'none'
                          }"\n                data-token="${
                            e.token
                          }"\n                data-email="${
                            e.email
                          }"\n              >usuń</a>\n              <a\n              id="user-${
                            e.token
                          }-activate"\n              href="/profil/${
                            e.token
                          }/aktywuj"\n              class="activate btn btn-mini btn-standard"\n              style="padding:1px 6px;display:${
                            e.is_active ? 'none' : 'block'
                          }"\n              data-token="${
                            e.token
                          }">aktywuj</a>\n            </div>\n          </div>\n          `
                      }\n        `
                  )
                  .join('\n')}\n        </div>\n        `
                : ''
            }\n    `;
          }
        },
        '/profil/:slug/aktywuj': {
          script: e => {},
          render: async e => {
            if (await j.activate(e)) {
              const t = await j.info(e);
              return (
                localStorage.setItem(
                  'userinfo',
                  JSON.stringify({
                    full_name: t.full_name,
                    email: t.email,
                    token: t.token
                  })
                ),
                D(),
                `\n      <div>\n      <h3>Użytkownik ${t.full_name} został aktywowany</h3>\n      </div>\n      `
              );
            }
            return '\n      <div>\n      <h3>Użytkownik nie mógł być aktywowany</h3>\n      </div>\n      ';
          }
        }
      },
      C = () => {
        const e = document.location.pathname.toLowerCase().split('/');
        return (
          'nowy' == e[2] && (e[3] = 'edytuj'),
          'aktualnosci' == e[1] && (e[1] = ''),
          { resource: e[1], slug: e[2], action: e[3] }
        );
      },
      H = e => {
        const t = new Object();
        return (
          window.location.search
            .slice(1)
            .split('&')
            .forEach(e => {
              e.split('=')[1] && (t[e.split('=')[0]] = e.split('=')[1]);
            }),
          e ? t[e] : t
        );
      },
      N = async e => {
        e && history.pushState(null, null, e);
        const t = document.querySelectorAll('[data-router]'),
          n = C(),
          i =
            (n.resource ? '/' + n.resource : '/') +
            (n.slug ? '/:slug' : '') +
            (n.action ? '/' + n.action : '');
        if ((c(!1), a(), o.hide(), window.scrollTo(0, 0), q[i])) {
          const e = document.getElementById('main'),
            t = await q[i].render(n.slug);
          void 0 !== t &&
            ((e.innerHTML = t), q[i].script && q[i].script(n.slug), p());
        } else v.render(404), console.log('E2');
        l(!1),
        (e => {
          const t = C().resource;
          e.forEach(e => {
            '/' + t === e.pathname
              ? (e.classList.add('active'),
              (document.title = 'Gazeta.org - ' + e.innerText))
              : e.classList.remove('active');
          });
        })(t);
      },
      M = e => {
        e.forEach(e => {
          e.addEventListener('click', t => {
            t.preventDefault(), t.ctrlKey ? window.open(e.href) : N(e.href);
          });
        });
      };
    var A = async () => {
      const e = await (async () => {
          let e = await fetch('/static/js/components/imieniny.json', {
            method: 'GET',
            headers: h()
          });
          return 200 === e.status ? e.json() : null;
        })(),
        t = await (async () => {
          try {
            const e = await fetch(
              'https://api.openweathermap.org/data/2.5/weather?q=poznan&units=metric&APPID=ec3d062501cacbd5024447564666ca9e'
            );
            if (200 === e.status) return e.json();
          } catch {
            return null;
          }
        })(),
        n = new Date(),
        a = `\n    ${n.toLocaleString('pl', {
          weekday: 'long'
        })},\n    ${n.getDate()}\n    ${n.toLocaleString('pl', {
          month: 'long'
        })}`,
        i = e.filter(
          e => e.day == n.getDate() && e.month == n.getMonth() + 1
        )[0].names,
        o = document.createElement('script');
      (o.src = 'https://kit.fontawesome.com/a076d05399.js'),
      document.head.appendChild(o);
      document.getElementById(
        'top'
      ).innerHTML = `\n    <div class="datetime-namedays" style="font-weight:bold">\n      <div class="datetime">${a}. </div>\n      <div class="namedays">Imieniny:</div>\n      <div class="namedays" style="font-weight:normal">${i.replaceAll(
        ' ',
        ', '
      )}</div>\n    </div>\n    <div class="weather">\n    ${
        t
          ? `\n      <i class="fas fa-${
            {
              '01d': 'sun',
              '02d': 'cloud-sun',
              '03d': 'cloud',
              '04d': 'cloud-meatball',
              '09d': 'cloud-showers-heavy',
              '10d': 'cloud-sun-rain',
              '11d': 'poo-storm',
              '13d': 'snowflake',
              '50d': 'smog',
              '01n': 'moon',
              '02n': 'cloud-moon',
              '03n': 'cloud',
              '04n': 'cloud-meatball',
              '09n': 'cloud-showers-heavy',
              '10n': 'cloud-moon-rain',
              '11n': 'poo-storm',
              '13n': 'snowflake',
              '50n': 'smog'
            }[t.weather[0].icon]
          }"></i>\n      <div style="display:inline;margin-left:2px">${Math.round(
            t.main.temp
          )}°c</div>\n      `
          : ''
      }\n    </div>\n  `;
    };
    var P = () => {
      const e = document.getElementsByTagName('header')[0];
      (document.getElementById('nav-main').innerHTML =
        '\n    <li> <a href="/" data-router="menu">Aktualności</a> </li>\n    <li> <a href="/kultura" data-router="menu">Kultura</a> </li>\n    <li> <a href="/sport" data-router="menu">Sport</a> </li>\n    <li> <a href="/ogloszenia" data-router="menu">Ogłoszenia</a> </li>\n    <li> <a href="/kontakt" data-router="menu">Kontakt</a> </li>\n  '),
      (document.getElementById('nav-right').innerHTML =
          '\n    <li id="search"><i class="fa fa-search" aria-hidden="true"></i><input type="text" / style="width:0px"><a href="/szukaj">Szukaj</a></li>\n    <li id="login-group"></li>\n  '),
      (document.getElementById('nav-mobile').innerHTML =
          '\n    <li id="mobile-search"><input type="text" /><a href="/szukaj">Szukaj</a></li>\n    <li> <a href="/" data-router="menu">Aktualności</a> </li>\n    <li> <a href="/kultura" data-router="menu">Kultura</a> </li>\n    <li> <a href="/sport" data-router="menu">Sport</a> </li>\n    <li> <a href="/ogloszenia" data-router="menu">Ogłoszenia</a> </li>\n    <li> <a href="/kontakt" data-router="menu">Kontakt</a> </li>\n    <li id="mobile-login-group"><a href="/login" class="login-button">Login</a></li>\n    <li id="mobile-menu-last"><div id="close-mobile-menu"></div></li>\n  ');
      const t = document.getElementById('hamburger'),
        n = document.querySelectorAll('[data-router]'),
        s = document.querySelector('#search input'),
        r = document.querySelector('#mobile-search input');
      M(n),
      (window.onscroll = () => {
        window.pageYOffset >= 50
          ? e.classList.add('stick')
          : e.classList.remove('stick');
      }),
      document
        .getElementById('close-mobile-menu')
        .addEventListener('click', () => a()),
      t.addEventListener('click', () => {
        t.classList.contains('openmenu') ? a() : i();
      }),
      s.addEventListener('keyup', function(e) {
        13 === e.keyCode &&
            (e.preventDefault(), document.querySelector('#search a').click());
      }),
      document.querySelector('#search a').addEventListener('click', e => {
        e.preventDefault(),
        'inline-block' != s.style.display
          ? o.show()
          : ('' !== s.value &&
                  (N('/szukaj/' + s.value),
                  (s.value = ''),
                  window.scrollTo(0, 0)),
          o.hide());
      }),
      r.addEventListener('keyup', function(e) {
        13 === e.keyCode &&
            (e.preventDefault(),
            document.querySelector('#mobile-search a').click());
      }),
      document
        .querySelector('#mobile-search a')
        .addEventListener('click', e => {
          e.preventDefault(),
          '' !== r.value &&
                (N('/szukaj/' + r.value),
                window.scrollTo(0, 0),
                (r.value = ''),
                a());
        }),
      (e => {
        let t = 0,
          n = 0;
        const o = document.getElementById('close-mobile-menu');
        e.addEventListener('touchstart', function(e) {
          const a = e.changedTouches[0];
          (n = 0), (t = parseInt(a.clientY));
        }),
        e.addEventListener('touchmove', function(e) {
          const a = e.changedTouches[0];
          (n = parseInt(a.clientY) - t), e.preventDefault();
        }),
        e.addEventListener('touchend', function() {
          n > 100 && i();
        }),
        o.addEventListener('touchstart', function(e) {
          const a = e.changedTouches[0];
          (n = 0), (t = parseInt(a.clientY));
        }),
        o.addEventListener('touchmove', function(e) {
          const a = e.changedTouches[0];
          (n = parseInt(a.clientY) - t), e.preventDefault();
        }),
        o.addEventListener('touchend', function() {
          n <= 0 && a();
        });
      })(e);
    };
    window.addEventListener('popstate', () => N()),
    document.addEventListener('DOMContentLoaded', async () => {
      P(),
      D(),
      A(),
      L(),
      (() => {
        if (!localStorage.getItem('acceptCookies')) {
          const e = document.createElement('DIV');
          (e.style.position = 'fixed'),
          (e.style.width = '100%'),
          (e.style.height = '40px'),
          (e.style.bottom = '0px'),
          (e.style.backgroundColor = 'rgba(255, 255, 255, 0.8)'),
          (e.style.padding = '10px'),
          (e.style.borderTop = '1px solid #234567'),
          (e.innerHTML =
                  '\n    <div style="float:left;width:calc(100% - 90px);padding:10px;text-align:center">\n      Strona wykorzystuje Biszkopty...\n    </div>\n    <div style=\'float:left\'>\n      <button id="accept-cookies" type="button" class="btn btn-primary">OK</button>\n    </div>\n    '),
          document.body.appendChild(e),
          document
            .getElementById('accept-cookies')
            .addEventListener('click', () => {
              localStorage.setItem('acceptCookies', !0), e.remove();
            });
        }
      })(),
      document.addEventListener('mousedown', e => {
        ('close' !== e.target.id && 'hidden' !== e.target.id) ||
              document.addEventListener(
                'mouseup',
                t => {
                  e.clientX === t.clientX && e.clientY === t.clientY && s();
                },
                { once: !0 }
              );
      }),
      document.addEventListener('keydown', e => {
        27 === e.keyCode && s();
      }),
      N();
    });
  }
]);
