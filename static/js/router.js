import HomeScreen from './screens/HomeScreen.js';
import ArticleScreen from './screens/ArticleScreen.js';
import ArticleFormScreen from './screens/ArticleFormScreen.js';
import SearchScreen from './screens/SearchScreen.js';
import AdvertisementsScreen from './screens/AdvertisementsScreen.js';
import AdvertisementFormScreen from './screens/AdvertisementFormScreen.js';
import ContactScreen from './screens/ContactScreen.js';
import ProfileScreen from './screens/ProfileScreen.js';
import ActivateScreen from './screens/ActivateScreen.js';
import ErrorScreen from './screens/ErrorScreen.js';
import { SearchInput, MobileMenu, Loading } from './effects.js';
import { mainNRight } from './utils.js';

const routes = {
  '/': HomeScreen,
  '/kultura': HomeScreen,
  '/sport': HomeScreen,
  '/art/:slug': ArticleScreen,
  '/artykul/:slug/edytuj': ArticleFormScreen,
  '/artykul/:slug': ArticleScreen,
  '/szukaj': SearchScreen,
  '/szukaj/:slug': SearchScreen,
  '/ogloszenia': AdvertisementsScreen,
  '/ogloszenie/:slug': AdvertisementsScreen,
  '/ogloszenie/:slug/edytuj': AdvertisementFormScreen,
  '/kontakt': ContactScreen,
  '/profil': ProfileScreen,
  '/profil/:slug/aktywuj': ActivateScreen
};
export const parseRequestUrl = () => {
  const url = document.location.pathname.toLowerCase();
  const request = url.split('/');
  if (request[2] == 'nowy') request[3] = 'edytuj';
  if (request[1] == 'aktualnosci') request[1] = '';
  return {
    resource: request[1],
    slug: request[2],
    action: request[3]
  };
};
export const getRequestSearch = param => {
  const params = new Object();
  window.location.search
    .slice(1)
    .split('&')
    .forEach(p => {
      p.split('=')[1] && (params[p.split('=')[0]] = p.split('=')[1]);
    });
  return param ? params[param] : params;
};
export const activeButton = navButtons => {
  const resource = parseRequestUrl().resource;
  navButtons.forEach(button => {
    if (`/${resource}` === button.pathname) {
      button.classList.add('active');
      document.title = `Gazeta.org - ${button.innerText}`;
    } else {
      button.classList.remove('active');
    }
  });
};
export const router = async url => {
  url && history.pushState(null, null, url);
  const navButtons = document.querySelectorAll('[data-router]');
  const request = parseRequestUrl();
  const parseUrl =
    (request.resource ? `/${request.resource}` : '/') +
    (request.slug ? '/:slug' : '') +
    (request.action ? `/${request.action}` : '');
  Loading.show(false);
  MobileMenu.hide();
  SearchInput.hide();
  window.scrollTo(0, 0);
  if (routes[parseUrl]) {
    const main = document.getElementById('main');
    const screenRender = await routes[parseUrl].render(request.slug);
    if (screenRender !== undefined) {
      main.innerHTML = screenRender;
      routes[parseUrl].script && routes[parseUrl].script(request.slug);
      mainNRight();
    }
  } else {
    ErrorScreen.render(404);
    console.log('E2');
  }
  Loading.hide(false);
  activeButton(navButtons);
};

export const routesAdd = hiperlinks => {
  hiperlinks.forEach(hiperlink => {
    hiperlink.addEventListener('click', event => {
      event.preventDefault();
      event.ctrlKey ? window.open(hiperlink.href) : router(hiperlink.href);
    });
  });
};
