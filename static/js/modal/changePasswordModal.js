import UserApi from '../api/UserApi.js';
import { Hidden } from '../effects.js';
// import loginModal from './loginModal.js';

const changePasswordModal = () => {
  document.getElementById('hidden').innerHTML = `
    <form action="/changepassword" method="post" id="changepassword-form">
      <h3>Zmień Hasło</h3>
      <p id="errorfield"></p>
      <input type="password" class="inputborder" placeholder="stare hasło" name="old" />
      <input type="password" class="inputborder" placeholder="nowe hasło" name="new" />
      <input type="password" class="inputborder" placeholder="powtórz nowe hasło" name="confirm" />
      <button type="submit" class="btn btn-primary">Zmień</button>
    </form>
  `;
  Hidden.show();
  const passwordForm = document.getElementById('changepassword-form');
  const fields = passwordForm.getElementsByTagName('input');
  passwordForm.addEventListener('submit', async event => {
    event.preventDefault();
    [...fields].forEach(field => {
      field.addEventListener('click', () => {
        field.classList.remove('warning');
      });
    });
    const passwordData = await UserApi.changePassword(
      Object.values(fields).reduce((obj, field) => {
        obj[field.name] = field.value;
        return obj;
      }, {})
    );
    if (passwordData.success) {
      Hidden.hide();
    } else {
      Object.keys(passwordData).forEach((key) => {
        fields[key] && fields[key].classList.add('warning');
      });
      document.getElementById('errorfield').innerHTML = 'Popraw poniższe błędy';
    }
  });
};
export default changePasswordModal;
