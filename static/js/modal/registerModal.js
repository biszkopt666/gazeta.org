import UserApi from '../api/UserApi.js';
import { Hidden } from '../effects.js';
import loginModal from './loginModal.js';

const registerModal = () => {
  document.getElementById('hidden').innerHTML = `
    <form action="/register" method="post" id="register-form">
      <h3>Rejestracja</h3>
      <p id="errorfield"></p>
      <input type="text" class="inputborder" placeholder="imię i nazwisko" name="full_name" />
      <input type="text" class="inputborder" placeholder="email" name="email" />
      <input type="password" class="inputborder" placeholder="hasło" name="password" />
      <input type="password" class="inputborder" placeholder="powtórz hasło" name="password2" />
      <button type="submit" class="btn btn-primary">Zarejestruj</button>
      <p class="small">Posiadasz już konto? <a href="/login">Przejdź do logowania</a></p>
    </form>
  `;
  Hidden.show();
  const signUpForm = document.getElementById('register-form');
  const fields = signUpForm.getElementsByTagName('input');
  signUpForm.addEventListener('submit', async event => {
    event.preventDefault();
    [...fields].forEach(field => {
      field.addEventListener('click', () => {
        field.classList.remove('warning');
      });
    });
    const signupData = await UserApi.register(
      Object.values(fields).reduce((obj, field) => {
        obj[field.name] = field.value;
        return obj;
      }, {})
    );
    if (signupData.token) {
      Hidden.hide();
    } else {
      Object.keys(signupData).forEach(key => {
        fields[key] && fields[key].classList.add('warning');
        // console.log(`Error ${i}: ${key}, Value: ${signupData[key]}`)
      });
      document.getElementById('errorfield').innerHTML = 'Popraw poniższe błędy';
    }
  });
  signUpForm.querySelector('a').addEventListener('click', event => {
    event.preventDefault();
    loginModal();
  });
};
export default registerModal;
