import { Hidden } from '../effects.js';

const confirmModal = (message, callback) => {
  document.getElementById('hidden').innerHTML = `
  <form action="" method="post" id="confirm-form">
    <h3>${message}</h3>
    <button type="submit" class="half-width btn btn-danger">Tak</button>
    <button type="button" class="half-width btn btn-primary">Nie</button>
  </form>
  `;
  Hidden.show();
  document.getElementById('confirm-form').addEventListener('submit', event => {
    event.preventDefault();
    Hidden.hide();
    callback(true);
  });
  document
    .querySelector('#confirm-form button[type="button"]')
    .addEventListener('click', () => {
      Hidden.hide();
      callback(false);
    });
};
export default confirmModal;
