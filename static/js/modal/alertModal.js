import { Hidden } from '../effects.js';

const alertModal = message => {
  document.getElementById('hidden').innerHTML = `
  <form action="" method="post" id="confirm-form">
    <h3>${message}</h3>
    <button type="submit" class="btn btn-danger">OK</button>
  </form>
  `;
  Hidden.show();
  document.getElementById('confirm-form').addEventListener('submit', event => {
    event.preventDefault();
    Hidden.hide();
    // callback(true);
  });
};
export default alertModal;
