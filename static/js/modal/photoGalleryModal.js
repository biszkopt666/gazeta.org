import { Hidden, PhotoGalleryAnimation } from '../effects.js';
import { touchPhotoGallery } from '../touchScreen.js';

const photoGalleryModal = (photos, index) => {
  let actualImage = parseInt(index);
  document.getElementById('hidden').innerHTML = `
    <div id="photogallery-main" > <img src="" /> </div>
    <div id="photogallery-previmg" > <i class="fa fa-chevron-left" aria-hidden="true"></i> </div>
    <div id="photogallery-nextimg" > <i class="fa fa-chevron-right" aria-hidden="true"></i> </div>
  `;
  const prevImgButton = document.getElementById('photogallery-previmg');
  const nextImgButton = document.getElementById('photogallery-nextimg');
  const bigImgTag = document.querySelector('#photogallery-main>img');
  const imgFrame = document.getElementById('photogallery-main');

  const activateImgButtons = imageIndex => {
    if (imageIndex > 0) {
      prevImgButton.dataset.index = imageIndex - 1;
      prevImgButton.style.opacity = 1;
    } else {
      prevImgButton.dataset.index = '';
      prevImgButton.style.opacity = 0;
    }
    if (imageIndex < photos.length - 1) {
      nextImgButton.dataset.index = parseInt(imageIndex) + 1;
      nextImgButton.style.opacity = 1;
    } else {
      nextImgButton.dataset.index = '';
      nextImgButton.style.opacity = 0;
    }
  };
  const resize = bigImg => {
    const imgWidth = bigImg.width;
    const imgHeight = bigImg.height;
    let newWidth = 0;
    let newHeight = 0;
    if (bigImg.width > window.innerWidth - 40) {
      newWidth = window.innerWidth - 40;
      newHeight = (window.innerWidth - 40) / (imgWidth / imgHeight);
    } else {
      newWidth = imgWidth;
      newHeight = imgHeight;
    }
    if (newHeight > window.innerHeight - 40) {
      newWidth = (window.innerHeight - 40) * (imgWidth / imgHeight);
      newHeight = window.innerHeight - 40;
    }
    bigImgTag.style.width = newWidth + 'px';
    bigImgTag.style.height = newHeight + 'px';
  };
  const Load = imageIndex => {
    if (imageIndex !== '') {
      PhotoGalleryAnimation.hide(actualImage, imageIndex, imgFrame);
      const bigImg = new Image();
      bigImg.src = photos[imageIndex].original;
      bigImg.addEventListener('load', () => {
        resize(bigImg);
        activateImgButtons(imageIndex);
        PhotoGalleryAnimation.show(actualImage, imageIndex, imgFrame);
        bigImgTag.src = bigImg.src;
        actualImage = parseInt(imageIndex);
      });
    }
  };
  [prevImgButton, nextImgButton].forEach(button => {
    button.addEventListener('click', () => {
      Load(button.dataset.index);
    });
  });
  touchPhotoGallery(imgFrame, async callback => {
    if (callback) {
      if (callback === 'next' && actualImage < photos.length - 1) {
        Load(actualImage + 1);
      } else if (callback === 'prev' && actualImage > 0) {
        Load(actualImage - 1);
      } else {
        imgFrame.style.left = '50%';
      }
    }
  });
  document.onkeydown = event => {
    switch (event.keyCode) {
    case 37:
      prevImgButton.click();
      break;
    case 39:
      nextImgButton.click();
      break;
    }
  };
  Load(index);
  Hidden.show();
};
export default photoGalleryModal;
