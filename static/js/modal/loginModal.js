import UserApi from '../api/UserApi.js';
// import { cookie } from '../utils.js';
import { Hidden } from '../effects.js';
import loginComponent from '../components/loginComponent.js';
import registerModal from './registerModal.js';
import alertModal from './alertModal.js';
import { router } from '../router.js';

const loginModal = () => {
  document.getElementById('hidden').innerHTML = `
    <form action="/login" method="post" id="login-form">
      <h3>Logowanie</h3>
      <p id="errorfield"></p>
      <input type="text"  class="inputborder" placeholder="email" name="email" />
      <input type="password" class="inputborder" placeholder="hasło" name="password" />
      <button type="submit" class="btn btn-primary">Login</button>
      <p class="small">Nie posiadasz jeszcze konta? <a href="/rejestracja">Kliknij Tutaj!!!</a></p>
    </form>
  `;
  Hidden.show();
  const loginForm = document.getElementById('login-form');
  const fields = loginForm.getElementsByTagName('input');
  loginForm.addEventListener('submit', async event => {
    event.preventDefault();
    [...fields].forEach((field) => {
      field.addEventListener('click', () => {
        field.classList.remove('warning');
      });
    });
    const loginData = await UserApi.login(
      fields['email'].value,
      fields['password'].value
    );
    if (loginData.token) {
      // console.log('token', loginData.token)

      if (loginData.active) {
        const userInfo = await UserApi.info(loginData.token);
        localStorage.setItem(
          'userinfo',
          JSON.stringify({
            full_name: userInfo.full_name,
            email: userInfo.email,
            token: userInfo.token
          })
        );
        Hidden.hide();
        router();
      } else {
        alertModal('Użytkownik jest nieaktywny');
      }
      loginComponent();
    } else {
      Object.keys(loginData).forEach((key) => {
        fields[key] && fields[key].classList.add('warning');
      });
      document.getElementById('errorfield').innerHTML = 'Popraw poniższe błędy';
    }
  });
  loginForm.querySelector('a').addEventListener('click', event => {
    event.preventDefault();
    registerModal();
  });
};
export default loginModal;
