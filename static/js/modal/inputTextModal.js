import { Hidden } from '../effects.js';

const inputTextModal = (message, callback) => {
  document.getElementById('hidden').innerHTML = `
  <form action="" method="post" id="form-inputtext">
    <h3>${message}</h3>
    <input type="text"  class="inputborder" name="title" />
    <button type="submit" class="half-width btn btn-danger">OK</button>
    <button type="button" class="half-width btn btn-primary">Anuluj</button>
  </form>
  `;
  Hidden.show();
  const input = document.querySelector('#form-inputtext input');
  document
    .getElementById('form-inputtext')
    .addEventListener('submit', event => {
      event.preventDefault();
      if (input.value) {
        callback(input.value);
        Hidden.hide();
      } else {
        input.classList.add('warning');
      }
    });
  document
    .querySelector('#form-inputtext button[type="button"]')
    .addEventListener('click', () => {
      Hidden.hide();
      callback(false);
    });
};
export default inputTextModal;
