const ContactScreen = {
  render: async () => {

    return `
      <div class="col-sm-1 col-lg-2">
        <iframe
          style="width:100%;height:400px"
          frameborder="0"
          scrolling="no"
          marginheight="0"
          marginwidth="0"
          src="https://www.openstreetmap.org/export/embed.html?bbox=106.91067069768907%2C47.923514192910204%2C106.91421121358873%2C47.92508869784562&amp;layer=mapnik&amp;marker=47.924301451368706%2C106.91244095563889"
          style="border: 1px solid black: width:200px"
        ></iframe>
        <br/>

      </div>
      <div class="col-sm-1 col-lg-2 address" style="text-align:center">
        <h3>Kontakt</h3>
        <p>17, Baga Street, Gandan, Ułan Bator, </p>
        <p>Sukhbaatar Duureg, 210646, Mongolia</p>
        <p>Email: <a href="mailto:czyngis@yuan.mn">mailto:czyngis@yuan.mn</a></p>
      </div>
    `
  }
}
export default ContactScreen
