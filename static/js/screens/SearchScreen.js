import Api from '../Api.js';
import { routesAdd} from '../router.js'

const SearchScreen = {
  script: (props) => {
    routesAdd([...document.querySelectorAll('#main a')]);
  },
  render: async (props) => {
    const newsList = await Api.getList('articles', {search: props || ''});
    const keyword = props.replace('%20',' ');
    const titleConvert = (text) => {
      const pos = text.toLowerCase().indexOf(keyword);
      if (pos>0) {
        return `${text.slice(0,pos)}<span class="search-fragment">${text.slice(pos,pos+keyword.length)}</span>${text.slice(pos+keyword.length, text.length)}`
      } else {
        return text
      }
    }
    const contentConvert = (text) => {
      const pos = text.toLowerCase().indexOf(keyword);
      let start = 0;
      let end = text.length;
      if (pos > 100) start=pos-100;
      if (text.length-pos-keyword.length>100) end = pos+keyword.length+100
      if (pos>=0) {
        return `...${text.slice(start,pos)}<span class="search-fragment">${text.slice(pos,pos+keyword.length)}</span>${text.slice(pos+keyword.length, end)}...`
      } else {
        if(text.length>200) {
          return `...${text.slice(parseInt((text.length/2)-100), parseInt((text.length/2)+100))}...`
        } else {
          return text
        }
      }
    }
    return `
    <div>
      <h3>Wyniki wyszukiwania dla "${keyword}"</h3>
      ${(newsList['results']?newsList.results:newsList).map(news => `
          <article>
          <a href='/artykul/${news.slug}'><h3>${titleConvert(news.title)}</h3></a>
          <p>${contentConvert(news.content)}<p>
          </article>
          `).join('\n')}
    </div>
    `
  }
}
export default SearchScreen
