import { translateCategory} from '../utils.js'
import {router, routesAdd} from '../router.js';
import rightComponent from '../components/rightComponent.js';
import { Loading } from '../effects.js';
import ErrorScreen from './ErrorScreen.js';
import Api from '../Api.js';

const ArticleFormScreen = {
  script: async(slug) => {
    routesAdd([document.querySelector('#go-back a')]);
    const articleForm = document.getElementById('article-form');
    if (articleForm) {
      const select = articleForm.querySelector('select')
      if (select.dataset.selected) select.value = select.dataset.selected.slice(0,1)
      const cover = articleForm.querySelector('input[name="cover"]')
      cover.addEventListener('change', ()=>{
        const name = cover.files[0].name.length>18
        ?`${cover.files[0].name.slice(0,10)}. .${cover.files[0].name.slice(cover.files[0].name.length-5,cover.files[0].name.length)}`
        : cover.files[0].name;
        articleForm.querySelector('label[for="cover"]').innerText = name;
      });
      articleForm.addEventListener('submit', async (event)=>{
        event.preventDefault();
        let data = new FormData();
        data.append('title', articleForm.querySelector('input[name="title"]').value);
        data.append('content', articleForm.querySelector('textarea').value);
        data.append('category', articleForm.querySelector('select').value);
        cover.files[0] && data.append('cover', cover.files[0]);
        Loading.show();
        const response = (slug=='nowy')?await Api.create('articles',data):await Api.update('articles', data, slug);
        if(response && response.slug) {
          router(`/artykul/${response.slug}`);
          rightComponent();
        } else {
          Object.keys(response).forEach((key, i) => {
            document.getElementsByName(key)[0].classList.add('warning');
            Loading.hide();
          });
        }
      });
    }
  },
  render: async(slug) => {
    const article = (slug==='nowy'||!slug)?({title:'',content:'',edit:true}):(await Api.get('articles',slug))
    if (!article.edit || !JSON.parse(localStorage.getItem('userinfo'))) {
      Error404Screen.render(403);
    } else {
      document.title = `Gazeta.org - ${article.title?`Edytuj: ${article.title}`:'Nowy'}`;
      return `
      <div id="go-back">
      <a href="/${article.slug?`artykul/${article.slug}`:''}" >
      <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>
      </a>
      </div>
        <form action="/article/new" method="post" class="embed-form" id="article-form">
          <input type="text" class="inputborder" name="title" value="${(article)?(`${article.title}`):(``)}" />
          <textarea name="content" class="inputborder" >${(article)?(`${article.content}`):(``)}</textarea>
          <label for="cover" class="image-upload"> <i class="fas fa-upload"></i> Dodaj okładkę</label>
          <input type="file" name="cover" id="cover" />
          <div style="float: right; margin-right: -21px;" id="form-buttons">
            <select ${(article.category)?(`data-selected="${article.category}"`):(``)}>
              <option value="n" selected >Aktualności</option>
              <option value="c">Kultura</option>
              <option value="s">Sport</option>
            </select>
            <button type="submit" class="btn btn-primary">Gotowe</button>
          </div>
        </form>
      `
    }
  }
}
export default ArticleFormScreen
