import UserApi from '../api/UserApi.js';
import inputTextModal from '../modal/inputTextModal.js';
import changePasswordModal from '../modal/changePasswordModal.js';
import loginComponent from '../components/loginComponent.js';

const ActivateScreen = {
  script: (token) => {
  },
  render: async(token) => {
    const activate = await UserApi.activate(token)
    if (activate) {
      const user = await UserApi.info(token);
      localStorage.setItem('userinfo' ,JSON.stringify({
        'full_name':user.full_name,
        'email':user.email,
        'token':user.token,
      }));
      loginComponent();
      return `
      <div>
      <h3>Użytkownik ${user.full_name} został aktywowany</h3>
      </div>
      `;
    } else {
      return `
      <div>
      <h3>Użytkownik nie mógł być aktywowany</h3>
      </div>
      `;
    }
  }
}
export default ActivateScreen;
