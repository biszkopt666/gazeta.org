import { translateCategory} from '../utils.js'
import {router, routesAdd} from '../router.js';
import rightComponent from '../components/rightComponent.js';
import inputTextModal from '../modal/inputTextModal.js';
import { Loading } from '../effects.js';
import ErrorScreen from './ErrorScreen.js';
import Api from '../Api.js';

const AdvertisementFormScreen = {
  script: async(slug) => {
    routesAdd([document.querySelector('#go-back a')]);
    const advertisementForm = document.getElementById('advertisement-form');
    if (advertisementForm) {
      const select = advertisementForm.querySelector('select');
      select.addEventListener('change', (event)=>{
        event.preventDefault();
        if(select.value === 'add') {
          inputTextModal("Wpisz nazwę kategorii", async(text) => {
            if (text) {
              let data = new FormData();
              data.append('name', text);
              const response = await Api.create('advertisementsCategories', data);
              if(response.slug) {
                const newCategory = document.createElement('option')
                newCategory.value=response.slug;
                newCategory.innerHTML = response.name;
                select.prepend(newCategory);
                select.selectedIndex = 0;
              }
            }
          });
        }
      })
      advertisementForm.addEventListener('submit', async (event)=>{
        event.preventDefault();
        let data = new FormData();
        data.append('title', advertisementForm.querySelector('input[name="title"]').value);
        data.append('content', advertisementForm.querySelector('textarea').value);
        data.append('category', advertisementForm.querySelector('select').value);
        data.append('price', advertisementForm.querySelector('input[name="price"]').value);
        Loading.show();
        const response = (slug=='nowy'||!slug)?await Api.create('advertisements', data):await Api.update('advertisements', data, slug)
        if(response.slug) {
          router(`/ogloszenie/${response.slug}`);
          rightComponent();
        } else {
          Object.keys(response).forEach((key, i) => {
            document.getElementsByName(key)[0].classList.add('warning');
            Loading.hide();
          });
        }
      })
    }
  },
  render: async(slug) => {
    const advertisement = (slug==='nowy'||!slug)?({title:'',content:'',edit:true}):(await Api.get('advertisements', slug))
    const categories = await Api.getList('advertisementsCategories');
    if (!JSON.parse(localStorage.getItem('userinfo'))) {
      Error404Screen.render(403);
    } else {
      return `
      <div id="go-back">
      <a href="/${advertisement.slug?`/ogloszenie/${advertisement.slug}`:''}" >
      <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>
      </a>
      </div>
        <form action="/advertisement/new" method="post" class="embed-form" id="advertisement-form">
          <input type="text" class="inputborder" name="title" value="${(advertisement)?(`${advertisement.title}`):(``)}" />
          <textarea name="content" class="inputborder" >${(advertisement)?(`${advertisement.content}`):(``)}</textarea>
          <select name="category">
                ${categories.map(category=>`
            <option value="${category.slug}">${category.name}</option>
                  `).join('\n')}
            <option value="add" id="add-category">+ Dodaj</option>
          </select>
          <input type="number" class="inputborder" name="price" placeholder="cena" value="${(advertisement)?(`${advertisement.price}`):(``)}" />
          <button type="submit" class="btn btn-primary adv-submit">Gotowe</button>
        </form>
      `
    }
  }
}
export default AdvertisementFormScreen
