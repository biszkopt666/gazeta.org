import {translateCategory} from '../utils.js'
import confirmModal from '../modal/confirmModal.js';
import {parseRequestUrl, getRequestSearch, routesAdd} from '../router.js'
import {Loading} from '../effects.js';
import Api from '../Api.js';

const HomeScreen = {
  script: () => {
    routesAdd([...document.getElementsByClassName('title')]);
    routesAdd([...document.getElementsByClassName('edit')]);
    routesAdd([...document.querySelectorAll('a.cover-mini')]);
    routesAdd([...document.querySelectorAll('#paginator a')]);
    let maxHeight = 0;
    [...document.getElementsByClassName('brick')].forEach((article, i) => {
      article.querySelector('.image').style.height = article.clientHeight - 45 - article.querySelector('h4').clientHeight + 'px'
      if (article.clientHeight > maxHeight) { maxHeight = article.clientHeight }
    });
    [...document.getElementsByClassName('brick')].forEach((article, i) => {
      if(maxHeight != article.clientHeight) {
        article.querySelector('.image').style.height = article.querySelector('.image').clientHeight+(maxHeight - article.clientHeight) +'px';
      }
    });
    [...document.getElementsByClassName('delete')].forEach((deleteButton, i) => {
      deleteButton.addEventListener('click', event=>{
        event.preventDefault();
        confirmModal('Czy na pewno chcesz usunąć ten artykuł?', async (confirmed) => {
          if (confirmed) {
            Loading.show();
            const deletedItem = await Api.delete('articles', event.target.pathname.split('/')[2]);
            if(deletedItem.status == '204') {
              const photos = await Api.getList('photos', {category:event.target.pathname.split('/')[2]});
              [...photos].forEach((photo) => {
                Api.delete('photos',photo.id);
              });
              router();
            } else {
              Loading.hide();
            }
          }
        });
      })
    });
  },
  render: async () => {
    const offset = getRequestSearch('strona')?(getRequestSearch('strona')-1)*limit:0;
    const user = JSON.parse(localStorage.getItem('userinfo'));
    const limit = user?59:60;
    const newsList = await Api.getList('articles', {
      category: translateCategory(parseRequestUrl().resource || 'aktualnosci').slice(0,1),
      limit: limit,
      offset: offset,
    })
    if (newsList) {
      const numPages = Math.ceil(newsList.count/limit)
      const currentPage = parseInt(getRequestSearch('strona')?getRequestSearch('strona'):1);
      return `
      <div id="main-inner">
      ${user?`
        <article class="brick add-brick">
          <div class="image" style="display:table">

            <a href="/artykul/nowy" class="edit"><i class="fas fa-plus"></i></a>
            <h4></h4>
          </div>
        </article>
        `:''}
      ${(newsList['results']?newsList.results:newsList).map(news => `
        <article class="brick">
        ${news.edit
          ? `
          <div class="buttons">
            <a href="/artykul/${news.slug}/edytuj" class="edit btn btn-primary btn-mini">Edytuj</a>
            <a href="/artykul/${news.slug}/usun" class="delete btn btn-danger btn-mini">Usun</a>
          </div>
          ` : '' }
          <a href="/artykul/${news.slug}" class="cover-mini">
          ${news.cover
            ?` <img class="image" src="${(news.cover_mini) || (news.cover)}" /> `
            :` <div class="image"></div> `}
          </a>
          <a class="title" href="/artykul/${news.slug}"><h4>${(news.title.length>75)?news.title.slice(0,75)+'...':news.title}</h4></a>
          <div class="author">Autor: ${news.author}</div>
        </article>
            `).join('\n')}
        <div
          id="paginator"
          style="display:table;width:100%;font-size:30px;letter-spacing:-4px"
        >
        ${newsList.previous?`
          <a href="?strona=${currentPage-1}">
            <div
            style="float:left;
            display:table"
            >
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </div>
          </a>
          `:''}
        ${newsList.next?`
          <a href="?strona=${currentPage+1}">
            <div
            style="float:right;margin-right:5px; display:table"
            >
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
          </a>
          `:''}
        </div>
      </div>
    `;
    }
  }
}
export default HomeScreen
