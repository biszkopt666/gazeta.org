import Api from '../Api.js';
import confirmModal from '../modal/confirmModal.js';
import photoGalleryModal from '../modal/photoGalleryModal.js';
import { Loading } from '../effects.js';
import {router, routesAdd} from '../router.js';
import {translateCategory} from '../utils.js'

const ArticleScreen = {
  script: (slug) => {
    const photos = JSON.parse(sessionStorage.getItem('photos'));
    sessionStorage.removeItem('photos');
    const photosArea = document.getElementById('article-thumbnails');
    const photosInput = document.getElementById('add-photos');
    const photosInputConfirm = document.getElementById('photos-input-confirm');
    const photosConfirmAlert = document.getElementById('photos-confirm-alert');
    document.title = `Gazeta.org - ${document.getElementById('title').innerHTML}`;
    routesAdd([document.querySelector('#go-back a')]);
    if(document.querySelector('.article .buttons')) {
      routesAdd([document.querySelector('.article .edit')]);
      document.querySelector('.article .delete').addEventListener('click', asyncevent=>{
        event.preventDefault();
        confirmModal('Czy na pewno chcesz usunąć ten artykuł?', async (confirmed) => {
          if (confirmed) {
            Loading.show();
            const deletedItem = await Api.delete('articles',event.target.pathname.split('/')[2]);
            if(deletedItem.status === 204) {
              const photos = await Api.getList('photos', {category:slug});
              [...photos].forEach((photo) => {
                Api.delete('photos',photo.id);
              });
              router('/'+event.target.dataset.category);
            } else {
              Loading.hide();
            }
          }
        });
      })
    };
    [...document.querySelectorAll('a.photo-link')].forEach((photo, i) => {
      photo.addEventListener('click', event=>{
        event.preventDefault();
        photoGalleryModal(photos,photo.dataset.index);
      })
    });
    const removePhoto = (actionArea) => {
      confirmModal('Czy na pewno chcesz usunąć to zdjęcie?', async (confirmed) => {
        if (confirmed) {
          Loading.show();
          const deletedItem = await Api.delete('photos',actionArea.dataset.id);
          if(deletedItem.status === 204) {
            actionArea.parentElement.remove();
            Loading.hide();
          }
        }
      });
    }
    if(photosInput) {
      photosInput.addEventListener('change',() => {
        const fileList = [...photosInput.files];
        const photosInputConfirmShow = () => {
          if(fileList.length>0) {
            photosInputConfirm.style.display = 'block';
            photosInputConfirm.style.opacity = 1;
            photosConfirmAlert.style.opacity = 1;
          } else {
            photosInputConfirm.style.display = 'none';
            photosInputConfirm.style.opacity = 0;
            photosConfirmAlert.style.opacity = 0;
          }
        };
        [...document.getElementsByClassName('new-photo')].forEach(photo => {
          photo.remove();
        })
        fileList.forEach((file, i) => {
          const src = URL.createObjectURL(file);
          const flagstone = document.createElement("DIV");
          flagstone.classList.add('flagstone','new-photo');
          // flagstone.style.paddingBottom = '5px';
          flagstone.innerHTML = `
          <a href="" style="" data-action="${photos.length+i}" data-i="${i}">
            <div class="photos-action" style="">
              <i class="fa fa-times" aria-hidden="true" style="color: #dc0000"></i>
            </div>
          </a>
          <a href="" data-index="${photos.length+i}">
            <img src="${src}" class="thumbnail" />
          </a>
          `;
          flagstone.addEventListener('click', event => {
            event.preventDefault();
          })
          const action = flagstone.getElementsByTagName('a')[0]
          action.addEventListener('click',event => {
            event.preventDefault();
            fileList.splice(action.dataset.i,1);
            [...document.querySelectorAll('[data-i]')].forEach((el) => {
              if (el.dataset.i>action.dataset.i) {
                el.dataset.i = el.dataset.i-1;
                el.dataset.index = el.dataset.index-1;
              }
            });
            action.parentElement.remove();
            photosInputConfirmShow();
          },{once:true})
          photosArea.appendChild(flagstone);
        });
        photosInputConfirmShow();
      });
      photosInputConfirm.addEventListener('click',()=>{
        const photosLengthOld = photos.length;
        [...photosInput.files].forEach(async(file,i) => {
          let data = new FormData();
          data.append('category', slug);
          data.append('original', file);
          const response = await Api.create('photos',data);
          if(response.mini) {
            const thumbnail = document.querySelector(`[data-index="${photosLengthOld+i}"]`);
            const actionArea = document.querySelector(`[data-action="${photosLengthOld+i}"]`);
            thumbnail.href = response.original;
            thumbnail.innerHTML = `<img src="${response.mini}" class="thumbnail">`;
            thumbnail.parentElement.classList.remove('new-photo');
            // thumbnail.parentElement.style.paddingBottom = 'unset';
            photos.push({id:response.id, category:response.category, original:response.original, mini:response.mini});
            // actionArea.style.display = 'block';
            thumbnail.addEventListener('click',event=>{
              event.preventDefault();
              photoGalleryModal(photos,thumbnail.dataset.index);
            });
            actionArea.addEventListener('click',event=>{
              event.preventDefault();
              removePhoto(actionArea);
            });
          };
        });
        photosInputConfirm.style.display = 'none';
        photosInputConfirm.style.opacity = 0;
        photosConfirmAlert.style.opacity = 0;
      });
    };
    [...document.getElementsByClassName('photo-remove')].forEach((button) => {
      button.addEventListener('click', event=>{
        event.preventDefault();
        removePhoto(button);
      });
    });
  },
  render: async(slug) => {
    const article = await Api.get('articles',slug)
    const photos = await Api.getList('photos', {category:slug})
    sessionStorage.setItem('photos',JSON.stringify(photos))
    const contentFormat = (text) => {
      let p = ''
      text.split('\n').forEach((line) => {
        if (line.length>1) p += `<p>${line}</p>`
      })
      return p;
    }
    if (article) {
      return `
      <div>
        <div id="go-back">
          <a href="/${translateCategory(article.category)}" >
            <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>
          </a>
        </div>
        <div class="article">
        ${article.edit
        ? `
          <div class="buttons">
            <a href="/artykul/${article.slug}/edytuj" class="edit btn btn-primary btn-mini">Edytuj</a>
            <a href="/artykul/${article.slug}/usun" class="delete btn btn-danger btn-mini" data-category="${translateCategory(article.category)}">Usun</a>
          </div>
        ` : '' }
        ${(!article)?(`<h3 id="title">Artykuł nie istnieje</h3>${window.history.back()}`):(`
          <h3 id="title">${article.title}</h3>${article.cover?`<img src="${article.cover}" />`:''}
          <div class="main-content">${contentFormat(article.content)}</div>
        `)}
          <div class="photo-thumbnails" id="article-thumbnails" >
          ${article.edit?`
          <div id="photos-confirm-alert" style="width:100%;text-align:center;font-size:20px;font-weight:bold;color:#da0000;opacity:0">Potwierdź aby przesłać</div>
            <div class="flagstone">
                <div class="photos-action" style="opacity:0;display:none;cursor:pointer" id="photos-input-confirm">
                  <i class="fa fa-check" aria-hidden="true" style="color: #00ee00;"></i>
                </div>
              <label for="add-photos" class="image-upload thumbnail" style="padding:0px;text-align:center" >
                <i class="fas fa-upload" style="font-size:40px;margin-top:20px;"></i>
              </label>
              <input type="file" name="add-photos" id="add-photos" multiple />
            </div>
            `:''}
            ${photos.map((photo,i)=>`
            <div class="flagstone">
            ${article.edit?`
              <a href="/zdjecie/${photo.id}/usun" class="photo-remove" data-id="${photo.id}">
                <div class="photos-action">
                  <i class="fa fa-times" aria-hidden="true" style="color: #dc0000"></i>
                </div>
              </a>
              `:''}
              <a href="${photo.original}" class="photo-link" data-index="${i}"> <img src="${photo.mini}" class="thumbnail" ></a>
            </div>
            `).join('\n')}

          </div>
        </div>
      </div>
      `;
    }
  }
}
export default ArticleScreen
