import UserApi from '../api/UserApi.js';
import inputTextModal from '../modal/inputTextModal.js';
import changePasswordModal from '../modal/changePasswordModal.js';
import confirmModal from '../modal/confirmModal.js';
import { router } from '../router.js';

const ProfileScreen = {
  script: () => {
    const removeAccount = (button) => {
      confirmModal(`Czy na pewno chcesz usunąć konto ${button.dataset.email}?`, async (confirmed) => {
        if (confirmed) {
          const deleted = await UserApi.delete(button.dataset.token);
          if (deleted.token) {
            document.getElementById(`user-${deleted.token}-activate`).style.display='block';
            button.style.display = 'none';
          }
        }
      })
    };
    [...document.getElementsByClassName('activate')].forEach((button) => {
      button.addEventListener('click', async(event)=>{
        event.preventDefault();
        const activate = await UserApi.activate(button.dataset.token);
        if(activate.token) {
          document.getElementById(`user-${activate.token}-delete`).style.display='block';
          button.style.display = 'none';
        }
      })
    });
    [...document.getElementsByClassName('delete-account')].forEach((button) => {
      button.addEventListener('click', event=>{
        event.preventDefault();
        removeAccount(button)
      })
    });
    document.getElementById('change-password').addEventListener('click', event => {
      event.preventDefault();
      changePasswordModal();
    })
    document.getElementById('hard-logout').addEventListener('click', event => {
      event.preventDefault();
      confirmModal(`Czy na pewno chcesz się wylogować ze wszystkich urządzeń?`, async (confirmed) => {
        if (confirmed) {
          const logout = await UserApi.logout(true);
          logout&&router('/')
        }
      })
    })
    // let i = 0;
    // const id = window.setInterval(() => {
    //   if (window.location.pathname === '/profil') {
    //     i++;
    //     console.log(i)
    //   } else {
    //     clearInterval(id);
    //     console.log('wyczyscilem')
    //   }
    // }, 1000);

  },
  render: async() => {
    const user = await UserApi.info(localStorage.getItem('userinfo')?JSON.parse(localStorage.getItem('userinfo')).token:'');
    const userList = user.is_staff?(await UserApi.list(localStorage.getItem('userinfo')?JSON.parse(localStorage.getItem('userinfo')).token:'')):null;
    return `
    <div>
    <h3>Profil użytkownika ${user.full_name}</h3>
      <div class="buttons">
        <a href="/haslo/zmien" class="change-password btn btn-primary" id="change-password">Zmień Hasło</a>
        <a href="/logout/hard" id="hard-logout" class="hard-logout btn btn-primary">Wyloguj ze wszystkiego</a>
        <a href="/konto/usun" class="delete-account btn btn-danger" data-token="${user.token}" data-email="${user.email}">Usuń Konto</a> </div>
      ${userList?`
        <div class="table users-table" style='margin-top:40px'>
        ${userList.map(user => `
          ${user.is_staff?``:`
          <div class="row">
            <div>${user.full_name}</div>
            <div>${user.email}</div>
            <div style="width:40px">
              <a
                id="user-${user.token}-delete"
                href="/profil/${user.token}/usun"
                class="delete-account btn btn-mini btn-danger"
                style="padding:1px 15px;display:${user.is_active?'block':'none'}"
                data-token="${user.token}"
                data-email="${user.email}"
              >usuń</a>
              <a
              id="user-${user.token}-activate"
              href="/profil/${user.token}/aktywuj"
              class="activate btn btn-mini btn-standard"
              style="padding:1px 6px;display:${user.is_active?'none':'block'}"
              data-token="${user.token}">aktywuj</a>
            </div>
          </div>
          `}
        `).join('\n')}
        </div>
        `:''}
    `
  }
}
export default ProfileScreen
