import Api from '../Api.js';
import {router, routesAdd, getRequestSearch} from '../router.js';
import confirmModal from '../modal/confirmModal.js';
import { mainNRight } from '../utils.js';

const AdvertisementsScreen = {
  script: async(slug) => {
    const user = JSON.parse(localStorage.getItem('userinfo'));
    const activateButtons = () => {
      routesAdd([document.querySelector('#ad-detail .edit')]);
      document.querySelector('#ad-detail .delete').addEventListener('click', asyncevent=>{
        event.preventDefault();
        confirmModal('Czy na pewno chcesz to ogłoszenie?', async (confirmed) => {
          if (confirmed) {
            Loading.show();
            const deletedItem = await Api.delete('advertisements' ,event.target.pathname.split('/')[2]);
            if(deletedItem.status === 204) {
              document.getElementById('ad-detail').innerHTML = '';
            }
            Loading.hide();
          }
        });
      })
    }
    const activateList = () => {
      document.querySelectorAll('#ad-table a').forEach((link) => {
        link.addEventListener('click', async(event)=> {
          event.preventDefault();
          const advertisement = await AdvertisementsScreen.renderDetail(link.pathname.split('/')[2],user);
          document.getElementById('ad-detail').innerHTML = advertisement;
          user&&activateButtons();
          history.pushState(null, null, link.pathname+window.location.search);
          window.scrollTo(0,50);
          mainNRight();
        })
      });
    }
    routesAdd([...document.querySelectorAll('#paginator a')]);
    user&&routesAdd([document.getElementById('ad-add')]);
    activateList();
    (user&&slug)&&activateButtons();
    document.querySelector('.ad-form select').selectedIndex = 0;
    document.querySelectorAll('#ad-fields>*').forEach((field) => {
      field.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
          event.preventDefault();
          document.getElementById('ad-search').click();
        }
      });
    });
    document.getElementById('ad-search').addEventListener('click', async()=>{
      const filter = new Object;
      document.querySelectorAll('#ad-fields>*').forEach((field) => {
        field.value&&(filter[field.name]=field.value);
      });
      let url = '';
      if (filter && Object.keys(filter).length>0) {
        url +='?'
        Object.keys(filter).forEach((key, i) => {
          url += `${key}=${filter[key]}`
          if (!Object.is(Object.keys(filter).length - 1, i)) { url+='&' }
        });
      }
      router('/ogloszenia/'+url);
    });

  },
  renderDetail: async(slug, user) => {
    const advertisement = await Api.get('advertisements', slug);
    const contentFormat = (text) => {
      let p = ''
      text.split('\n').forEach((line) => {
        if (line.length>1) p += `<p>${line}</p>`
      })
      return p;
    }
    if(advertisement){
      return `
      ${user
      ? `
        <div class="buttons">
          <a href="/ogloszenie/${advertisement.slug}/edytuj" class="edit btn btn-primary btn-mini">Edytuj</a>
          <a href="/ogloszenie/${advertisement.slug}/usun" class="delete btn btn-danger btn-mini" data-category="${advertisement.category}">Usun</a>
        </div>
      ` : '' }
      <div class="underline-box">
        <h3>${advertisement.title}</h3>
        <div class="main-content">
          <p>${contentFormat(advertisement.content)}</p>
        </div>
        <div class="price-contact">
          <span>${advertisement.category_name}</span>
          <span style="float:right">Cena: ${String(advertisement.price).replace(/(\d)(?=(\d{3})+$)/g, '$1 ')} zł.</span>
        </div>
      </div>
      `;
    } else {
      return '';
    }

  },
  render: async(slug) => {
    const limit = 40;
    const filter = getRequestSearch();
    filter['limit'] = limit;
    filter['offset'] = getRequestSearch('strona')?(getRequestSearch('strona')-1)*limit:0;
    const user = JSON.parse(localStorage.getItem('userinfo'));
    const advertisementList = await Api.getList('advertisements', filter);
    const advertisement = slug?(await AdvertisementsScreen.renderDetail(slug, user)):false;
    const categories = await Api.getList('advertisementsCategories');
    if(advertisementList&&categories) {
      const currentPage = parseInt(getRequestSearch('strona')?getRequestSearch('strona'):1);
      const prevNextUrl = (num) => {
        const addr = window.location.search;
        if(addr) {
          if(addr.includes('strona')) {
            return addr.replace(`strona=${getRequestSearch('strona')}`,`strona=${num}`);
          } else {
            return `${addr}&strona=${num}`;
          }
        } else {
          return `?strona=${num}`
        }
      };
      return `
      <div style="over">
        <div class="ad-form underline-box">
          <div id="ad-fields">
            <input type="text" class="inputborder" name="item" placeholder="Czego szukasz?" />
            <select name="category" class="inputborder" >
              <option value="">Wszystko</option>
              ${categories.map(category=>`
              <option value="${category.slug}">${category.name}</option>
                `).join('\n')}
              </select>
            <input type="number" class="inputborder" name="price" placeholder="cena max." />
          </div>
          <div><input type="button" id="ad-search" class="btn btn-primary" value="Szukaj" ></input></div>
        </div>
        ${user?`
        <div>
          <a href="/ogloszenie/nowy" id="ad-add" style="color:white">
            <div class="btn btn-primary" style="text-align:center;margin-bottom:5px;">Dodaj</div>
          </a>
        </div>
        `:''}
        <div id="ad-detail">${slug?advertisement:''}</div>
        <div class="table adv-table" id="ad-table">
        ${advertisementList.count?`
          ${(advertisementList['results']?advertisementList.results:advertisementList).map(advertisement => `
          <div class="row">
            <div><a href="/ogloszenie/${advertisement.slug}">${advertisement.title}</a></div>
            <div class="cell-price">${String(advertisement.price).replace(/(\d)(?=(\d{3})+$)/g, '$1 ')} zł.</div>
          </div> `).join('\n')}
          `:`
          <h3>Brak ogłoszeń spełniających kryteria</h3>
          `}
        </div>
        <div
          id="paginator"
          style="display:table;width:100%;font-size:30px;letter-spacing:-4px"
        >
        ${advertisementList.previous?`
          <a href="/ogloszenia${prevNextUrl(currentPage-1)}">
            <div
            style="float:left;
            display:table"
            >
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </div>
          </a>
          `:''}
        ${advertisementList.next?`
          <a href="/ogloszenia${prevNextUrl(currentPage+1)}">
            <div
            style="float:right;margin-right:5px; display:table"
            >
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
          </a>
          `:''}
        </div>
      </div>
      `;
    }
  }
}
export default AdvertisementsScreen
