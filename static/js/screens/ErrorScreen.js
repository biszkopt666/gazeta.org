import { Loading } from '../effects.js';

const messages = {
  401:'Nie masz uprawnień do oglądania tej treści',
  404:'Strona o podanym adresie nie istnieje',

};

const ErrorScreen = {
  render: (status, message) => {
    Loading.hide();
    main.innerHTML = `
    <h1 class="error-page" style="text-align:center">
      Error: ${status?status:''} ${status?messages[status]:'Wystąpił nieznany błąd serwera'}
    </h1>
    `;
  },
  log: (status, message) => {

  },
  alert: (status, message) => {

  }
};

export default ErrorScreen;
