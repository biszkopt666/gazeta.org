import { getHeaders } from '../utils.js';
import ErrorScreen from '../screens/ErrorScreen.js';

const UserApi = {
  login: async (email, password) => {
    try {
      const response = await fetch(
        'http://localhost:8000/api/accounts/login/',
        {
          method: 'POST',
          body: JSON.stringify({ email: email, password: password }),
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }
      );
      console.log('Login Status', response.status);
      if (response.status === 200) {
        return response.json();
      } else {
        if (response.status === 400) {
          return response.json();
        } else {
          return null;
        }
      }
    } catch {
      ErrorScreen.render(false);
    }
  },
  logout: async force => {
    try {
      if (force) {
        await fetch(
          'http://localhost:8000/api/accounts/logout/',
          {
            method: 'POST',
            headers: getHeaders()
          }
        );
      }
      localStorage.removeItem('userinfo');
    } catch {
      ErrorScreen.render(false);
    }
  },
  register: async data => {
    try {
      const response = await fetch(
        'http://localhost:8000/api/accounts/register/',
        {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }
      );
      if (response.status === 200) {
        return response.json();
      } else {
        if (response.status === 400) {
          return response.json();
        } else {
          ErrorScreen.render(false);
        }
      }
    } catch {
      ErrorScreen.render(500);
    }
  },
  activate: async token => {
    try {
      const response = await fetch(
        `http://localhost:8000/api/accounts/activate/${token}/`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          }
        }
      );
      if (response.status === 200) {
        return response.json();
      } else {
        if (response.status === 204) {
          return null;
        } else {
          ErrorScreen.render(response.status);
        }
      }
    } catch {
      ErrorScreen.render(false);
    }
  },
  changePassword: async data => {
    try {
      const response = await fetch(
        'http://localhost:8000/api/accounts/changepassword/',
        {
          method: 'PUT',
          body: JSON.stringify(data),
          headers: getHeaders()
        }
      );
      if (response.status === 200) {
        return response.json();
      } else {
        if (response.status === 400) {
          return response.json();
        } else {
          ErrorScreen.render(false);
        }
      }
    } catch {
      ErrorScreen.render(false);
    }
  },
  info: async token => {
    try {
      const response = await fetch(
        'http://localhost:8000/api/accounts/userinfo/',
        {
          method: 'GET',
          headers: getHeaders({
            Authorization: `Token ${token}`
          })
        }
      );
      if (response.status === 200) {
        const json = await response.json();
        return json;
      } else {
        ErrorScreen.render(
          response.status,
          'Sprawdź czy użytkownik został aktywowany'
        );
        // localStorage.removeItem('userinfo')
      }
    } catch (error) {
      ErrorScreen.render(false);
    }
  },
  list: async token => {
    try {
      const response = await fetch(
        'http://localhost:8000/api/accounts/userlist/',
        {
          method: 'GET',
          headers: getHeaders({
            Authorization: `Token ${token}`
          })
        }
      );
      if (response.status === 200) {
        return response.json();
      } else {
        return null;
      }
    } catch (error) {
      ErrorScreen.render(false);
    }
  },
  delete: async token => {
    try {
      const response = await fetch(
        `http://localhost:8000/api/accounts/delete/${token}/`,
        {
          method: 'DELETE',
          headers: getHeaders()
        }
      );
      if (response.status === 200) {
        return response.json();
      } else {
        if (response.status === 204) {
          return null;
        } else {
          ErrorScreen.render(response.status);
        }
      }
    } catch {
      ErrorScreen.render(false);
    }
  }
};
export default UserApi;
