import {  getHeaders } from './utils.js';
import ErrorScreen from './screens/ErrorScreen.js';

const modelList = {
  articles: 'api/articles',
  advertisements: 'api/adios',
  advertisementsCategories: 'api/adios/category',
  photos: 'api/photos'
};

const Api = {
  getList: async (model, data) => {
    try {
      let url = `http://localhost:8000/${modelList[model]}/`;
      if (data && Object.keys(data).length > 0) {
        url += '?';
        Object.keys(data).forEach((key, i) => {
          url += `${key}=${data[key]}`;
          if (!Object.is(Object.keys(data).length - 1, i)) {
            url += '&';
          }
        });
      }
      const response = await fetch(url, {
        method: 'GET',
        headers: getHeaders()
      });
      if (response.status === 200) {
        return response.json();
      } else {
        ErrorScreen.render(response.status);
      }
    } catch (error) {
      ErrorScreen.render(false, 'Brak połączenia z serwerem');
      return false;
    }
  },
  get: async (model, slug) => {
    try {
      const response = await fetch(
        `http://localhost:8000/${modelList[model]}/${slug}`,
        {
          method: 'GET',
          headers: getHeaders()
        }
      );
      if (response.status === 200) {
        return response.json();
      } else {
        ErrorScreen.render(response.status, response.statusText);
        return null;
      }
    } catch (error) {
      ErrorScreen.render(false, 'Brak połączenia z serwerem');
    }
  },
  create: async (model, data) => {
    try {
      const response = await fetch(
        `http://localhost:8000/${modelList[model]}/`,
        {
          method: 'POST',
          body: data,
          headers: getHeaders({
            'Content-Type': ''
          })
        }
      );
      return response.json();
    } catch {
      ErrorScreen.render(false);
    }
  },
  update: async (model, data, slug) => {
    try {
      const response = await fetch(
        `http://localhost:8000/${modelList[model]}/${slug}`,
        {
          method: 'PUT',
          body: data,
          headers: getHeaders({
            'Content-Type': ''
          })
        }
      );
      if (response.ok) {
        return response.json();
      } else {
        if (response.status === 400) {
          return response.json();
        } else {
          ErrorScreen.render(response.status);
          return false;
        }
      }
    } catch {
      ErrorScreen.render(false);
    }
  },
  delete: async (model, slug) => {
    try {
      return await fetch(`http://localhost:8000/${modelList[model]}/${slug}/`, {
        method: 'DELETE',
        headers: getHeaders({
          'Content-Type': ''
        })
        // headers: getHeaders(),
      });
    } catch {
      ErrorScreen.render(false);
    }
  }
};
export const getWeather = async () => {
  try {
    const response = await fetch(
      'https://api.openweathermap.org/data/2.5/weather?q=poznan&units=metric&APPID=ec3d062501cacbd5024447564666ca9e'
    );
    if (response.status === 200) {
      return response.json();
    }
  } catch {
    return null;
  }
};
export const getNameDays = async () => {
  let response = await fetch('/static/js/components/imieniny.json', {
    method: 'GET',
    headers: getHeaders()
  });
  if (response.status === 200) {
    return response.json();
  } else {
    return null;
  }
};
export default Api;
