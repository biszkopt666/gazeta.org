export const MobileMenu = {
  hide: () => {
    const menu = document.getElementById('nav-mobile');
    const hamburger = document.getElementById('hamburger');
    if (hamburger.classList.contains('openmenu')) {
      hamburger.classList.remove('openmenu');
      let i = 20;
      const id = setInterval(() => {
        if (i == 0) {
          menu.style.display = 'none';
          clearInterval(id);
        } else {
          i--;
        }
        menu.style.opacity = 0.1 * i;
      }, 10);
      document.body.style.overflowY = 'auto';
    }
  },
  show: () => {
    const menu = document.getElementById('nav-mobile');
    document.getElementById('hamburger').classList.add('openmenu');
    let i = 0;
    menu.style.opacity = 0;
    menu.style.display = 'block';
    const id = setInterval(() => {
      if (i == 10) {
        clearInterval(id);
      } else {
        i++;
      }
      menu.style.opacity = 0.1 * i;
    }, 10);
    document.body.style.overflowY = 'hidden';
  }
};
export const SearchInput = {
  hide: () => {
    const input = document.querySelector('#search input');
    const widthStep = input.clienWidth > 150 ? 15 : 7;
    if (input.style.width != '0px') {
      let i = 20;
      const id = setInterval(() => {
        if (i == 0) {
          input.style.display = '';
          input.style.marginLeft = '0px';
          clearInterval(id);
        } else {
          i--;
        }
        input.style.width = widthStep * i + 'px';
        input.style.opacity = 0.05 * i;
      }, 10);
    }
  },
  show: () => {
    const input = document.querySelector('#search input');
    const widthStep =
      document.getElementById('nav-right').offsetLeft > 1030 ? 15 : 7;
    let i = 0;
    input.style.display = 'inline-block';
    input.style.marginLeft = '2px';
    const id = setInterval(() => {
      if (i == 20) {
        clearInterval(id);
        input.select();
      } else {
        i++;
      }
      input.style.width = widthStep * i + 'px';
      input.style.opacity = 0.05 * i;
    }, 10);
    document.getElementById('main').addEventListener(
      'click',
      () => {
        SearchInput.hide();
        MobileMenu.hide();
      },
      { once: true }
    );
  }
};
export const Hidden = {
  hide: () => {
    const hiddenField = document.getElementById('hidden');
    const close = document.getElementById('close');
    let i = 20;
    const id = setInterval(() => {
      if (i == 0) {
        hiddenField.style.display = 'none';
        close.style.display = 'none';
        hiddenField.innerHTML = '';
        document.body.style.overflowY = 'auto';
        clearInterval(id);
      } else {
        i--;
      }
      hiddenField.style.opacity = 0.1 * i;
      close.style.opacity = 0.1 * i;
    }, 10);
  },
  show: () => {
    const hiddenField = document.getElementById('hidden');
    if (hiddenField.style.display !== 'block') {
      const close = document.getElementById('close');
      let i = 0;
      hiddenField.style.opacity = 0;
      hiddenField.style.display = 'block';
      close.style.opacity = 0;
      close.style.display = 'block';
      document.body.style.overflowY = 'hidden';
      const id = setInterval(() => {
        if (i == 10) {
          clearInterval(id);
        } else {
          i++;
        }
        hiddenField.style.opacity = 0.1 * i;
        close.style.opacity = 0.1 * i;
      }, 10);
    }
  }
};
export const Loading = {
  hide: fadeInOut => {
    const loading = document.getElementById('loading');
    if (fadeInOut) {
      let i = 20;
      const id = setInterval(() => {
        if (i == 0) {
          loading.style.display = 'none';
          clearInterval(id);
        } else {
          i--;
        }
        loading.style.opacity = 0.1 * i;
      }, 10);
    } else {
      loading.style.display = 'none';
    }
  },
  show: fadeInOut => {
    const loading = document.getElementById('loading');
    let i = 0;
    loading.style.display = 'block';
    if (fadeInOut) {
      loading.style.opacity = 0;
      const id = setInterval(() => {
        if (i == 10) {
          clearInterval(id);
        } else {
          i++;
        }
        loading.style.opacity = 0.1 * i;
      }, 10);
    } else {
      loading.style.opacity = 1;
    }
  }
};
export const PhotoGalleryAnimation = {
  hide: (prev, next, el) => {
    el.style.opacity = 0;
  },
  show: (prev, next, el) => {
    const maxLeft = -(window.innerWidth / 2);
    const maxRight = window.innerWidth * (3 / 2);
    const step = window.innerWidth / 20;
    let i = 0;
    const id = setInterval(() => {
      if (i == 40) {
        el.style.left = '50%';
        clearInterval(id);
      } else {
        if (next < prev) {
          el.style.left = maxLeft + (i - 20) * step + 'px';
        }
        if (next > prev) {
          el.style.left = maxRight - (i - 20) * step + 'px';
        }
        el.style.opacity = 1;
        i++;
      }
    }, 5);
  }
};
export const NewAnimation = {
  hide: () => {},
  show: () => {}
};
