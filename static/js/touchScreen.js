import { MobileMenu } from './effects.js';

export const touchMenu = header => {
  // let startx = 0;
  let starty = 0;
  let dist = 0;
  const closeMobileMenu = document.getElementById('close-mobile-menu');
  header.addEventListener('touchstart', function(event) {
    const touchobj = event.changedTouches[0];
    dist = 0;
    starty = parseInt(touchobj.clientY);
    // startx = parseInt(touchobj.clientX);
  });
  header.addEventListener('touchmove', function(event) {
    const touchobj = event.changedTouches[0];
    dist = parseInt(touchobj.clientY) - starty;
    event.preventDefault();
  });
  header.addEventListener('touchend', function() {
    // const touchobj = event.changedTouches[0];
    dist > 100 && MobileMenu.show();
  });
  closeMobileMenu.addEventListener('touchstart', function(event) {
    const touchobj = event.changedTouches[0];
    dist = 0;
    starty = parseInt(touchobj.clientY);
  });
  closeMobileMenu.addEventListener('touchmove', function(event) {
    const touchobj = event.changedTouches[0];
    dist = parseInt(touchobj.clientY) - starty;
    event.preventDefault();
  });
  closeMobileMenu.addEventListener('touchend', function() {
    // const touchobj = event.changedTouches[0];
    dist <= 0 && MobileMenu.hide();
  });
};
export const touchPhotoGallery = (el, callback) => {
  const startPos = window.innerWidth / 2;
  let startx = 0;
  let dist = 0;
  el.addEventListener('touchstart', function(event) {
    const touchobj = event.changedTouches[0];
    dist = 0;
    startx = parseInt(touchobj.clientX);
    event.preventDefault();
  });
  el.addEventListener('touchmove', function(event) {
    const touchobj = event.changedTouches[0];
    dist = parseInt(touchobj.clientX) - startx;
    el.style.left = startPos + dist + 'px';
    event.preventDefault();
  });
  el.addEventListener('touchend', function() {
    // const touchobj = event.changedTouches[0];
    if (Math.abs(dist) > window.innerWidth / 2) {
      if (dist > 0) {
        callback('prev');
      } else {
        callback('next');
      }
    } else {
      el.style.left = '50%';
    }
  });
};
