import { Hidden } from './effects.js';
export const mainNRight = () => {
  if (window.innerWidth >= 800) {
    const rightInner = document.getElementById('right-inner');
    const right = document.getElementById('right');
    const main = document.getElementById('main');
    if (rightInner) {
      const elements = rightInner.querySelectorAll('a');
      [...elements].forEach(element => {
        element.style.display = 'unset';
      });
      let i = 1;
      while (main.offsetHeight < right.offsetHeight) {
        [...elements][elements.length - i].style.display = 'none';
        i++;
      }
    }
  }
};
export const bgEvents = () => {
  document.addEventListener('mousedown', event => {
    if (event.target.id === 'close' || event.target.id === 'hidden') {
      document.addEventListener(
        'mouseup',
        reevent => {
          if (
            event.clientX === reevent.clientX &&
            event.clientY === reevent.clientY
          )
            Hidden.hide();
        },
        { once: true }
      );
    }
  });
  document.addEventListener('keydown', event => {
    event.keyCode === 27 && Hidden.hide();
  });
};
export const Cookie = {
  get: name => {
    for (
      let i = 0;
      i < decodeURIComponent(document.cookie).split(';').length;
      i++
    ) {
      let c = decodeURIComponent(document.cookie).split(';')[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(`${name}=`) == 0) {
        return c.substring(`${name}=`.length, c.length);
      }
    }
    return '';
  },
  set: (name, value, days) => {
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    document.cookie = `${name}=${value};expires=${date.toUTCString()};path=/`;
  },
  delete: (name, value) => {
    var date = new Date();
    date.setSeconds(date.getSeconds() + 1);
    document.cookie = `${name}=${value};expires=${date.toUTCString()};path=/`;
  }
};
export const acceptCookies = () => {
  if (!localStorage.getItem('acceptCookies')) {
    const infoCookie = document.createElement('DIV');
    infoCookie.style.position = 'fixed';
    infoCookie.style.width = '100%';
    infoCookie.style.height = '40px';
    infoCookie.style.bottom = '0px';
    infoCookie.style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    infoCookie.style.padding = '10px';
    infoCookie.style.borderTop = '1px solid #234567';
    infoCookie.innerHTML = `
    <div style="float:left;width:calc(100% - 90px);padding:10px;text-align:center">
      Strona wykorzystuje Biszkopty...
    </div>
    <div style='float:left'>
      <button id="accept-cookies" type="button" class="btn btn-primary">OK</button>
    </div>
    `;
    document.body.appendChild(infoCookie);
    document.getElementById('accept-cookies').addEventListener('click', () => {
      localStorage.setItem('acceptCookies', true);
      infoCookie.remove();
    });
  }
};
export const translateCategory = category => {
  switch (category) {
  case 'aktualnosci':
    return 'news';
  case 'kultura':
    return 'culture';
  case 'sport':
    return 'sport';
  case 'news':
    return 'aktualnosci';
  case 'culture':
    return 'kultura';
  default:
    return category;
  }
};
export const closeModal = () => {
  const hidden = document.getElementById('hidden');
  hidden.style.display = 'none';
  document.getElementById('close').style.display = 'none';
  document.body.style.overflowY = 'auto';
  hidden.innerHTML = '';
};
export const convertString = string => {
  let s = string;
  s = s.replace(/ę/gi, 'e');
  s = s.replace(/ż/gi, 'z');
  s = s.replace(/ó/gi, 'o');
  s = s.replace(/ł/gi, 'l');
  s = s.replace(/ć/gi, 'c');
  s = s.replace(/ś/gi, 's');
  s = s.replace(/ź/gi, 'z');
  s = s.replace(/ń/gi, 'n');
  s = s.replace(/ą/gi, 'a');
  return s;
};
export const getHeaders = data => {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('X-Requested-With', 'XMLHttpRequest');
  try {
    headers.append(
      'Authorization',
      `Token ${JSON.parse(localStorage.getItem('userinfo')).token}`
    );
  } catch {
    localStorage.removeItem('userinfo');
  }
  headers.append('X-CSRFToken', Cookie.get('csrftoken'));
  if (data && Object.keys(data).length > 0) {
    Object.keys(data).forEach(key => {
      headers.delete(key);
      if (data[key] || data[key] !== '') {
        headers.append(key, data[key]);
      }
    });
  }
  return headers;
};
