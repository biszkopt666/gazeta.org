from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_204_NO_CONTENT
from rest_framework.generics import ListCreateAPIView, RetrieveAPIView, ListAPIView, CreateAPIView, UpdateAPIView, RetrieveDestroyAPIView
from .serializers import UserSerializer, LoginSerializer, UserCreateSerializer, ChangePasswordSerializer
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from django.contrib.auth import get_user_model
import datetime


class SignUp(CreateAPIView):
	permission_classes = [AllowAny]
	serializer_class = UserCreateSerializer

	def post(self, request, *args, **kwargs):
		model = get_user_model()
		data = self.request.data
		serializer = UserCreateSerializer(data=request.data)
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response({'user': serializer.data, 'token': str(Token.objects.get(user=serializer.instance.id))})
		return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

class Activate(RetrieveAPIView):
	permission_classes = [AllowAny]

	def get(self, request, *args, **kwargs):
		token = kwargs.get('token', None)
		try:
			user = Token.objects.get(key=kwargs.get('token', None)).user
			user.is_active = True
			user.save()
		except:
			return Response(status=HTTP_204_NO_CONTENT)
		return Response({'token': token}, status=HTTP_200_OK)

	def delete(self, request, *args, **kwargs):
		token = kwargs.get('token', None)
		try:
			user = Token.objects.get(key=kwargs.get('token', None)).user
			user.is_active = False
			user.save()
		except:
			return Response(status=HTTP_204_NO_CONTENT)
		return Response({'token': token}, status=HTTP_200_OK)



class Login(APIView):
	permission_classes = [AllowAny,]
	serializer_class = LoginSerializer

	def post(self, request, *args, **kwargs):
		data = request.data
		serializer = LoginSerializer(data=data)
		if serializer.is_valid(raise_exception=True):
			user = serializer.data
			token, created = Token.objects.get_or_create(user=get_user_model().objects.get(id=user['id']))
			return Response({ 'token': token.key, 'user_id': user['id'], 'email': user['email'], 'active': user['is_active'] })
		return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

class Logout(APIView):
	def get(self, request, format=None):
		request.user.auth_token.delete()
		return Response(status=HTTP_200_OK)

class UserList(ListAPIView):
	queryset = get_user_model().objects.all()
	serializer_class = UserSerializer
	permission_classes = [IsAdminUser,]

class UserView(RetrieveAPIView):
	permission_classes = [IsAuthenticated,]

	def get(self, request):
		if bool(request.user.is_anonymous):
			return Response(status=HTTP_204_NO_CONTENT)
		return Response(UserSerializer(request.user).data)

class ChangePasswordView(UpdateAPIView):
	serializer_class = ChangePasswordSerializer
	permission_classes = [IsAuthenticated,]

	def update(self, request, *args, **kwargs):
		user = request.user
		request.data['email'] = user.email
		serializer = self.get_serializer(data=request.data)
		if serializer.is_valid(raise_exception=True):
			serializer.save()
			return Response({'success': 'Password changed'}, status=HTTP_200_OK)
		return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
