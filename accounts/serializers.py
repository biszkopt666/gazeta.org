from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.authentication import BasicAuthentication
# from .models import User
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('full_name', 'email', 'is_staff', 'is_active', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        try:
            representation['token'] = Token.objects.get(user=instance.id).key or None
        except:
            representation['token'] = None
        return representation

class UserCreateSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(write_only=True)
    class Meta:
        model = User
        fields = ['email', 'full_name', 'password', 'password2']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_password2(self, value):
        data = self.get_initial()
        password = data.get('password')
        password2 = value
        if password != password2:
            raise ValidationError('Passwords must match')
        return value

    def create(self, request, *args, **kwargs):
        user = User( email = self.validated_data['email'], full_name = self.validated_data['full_name'], )
        user.set_password(self.validated_data['password'])
        user.save()
        token = Token.objects.get(user=user)
        return user

class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    token = serializers.CharField(allow_blank=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'password', 'token', 'is_active']
        extra_kwargs = {"password": {"write_only": True}}

    def validate(self, data):
        email = data.get('email', None)
        password = data['password']
        if not email:
            raise ValidationError('Email is required')
        user = User.objects.filter(email=email)
        if user.exists():
            user = user.first()
        else:
            raise ValidationError({'email': 'User does not exists'})
        if user:
            if not user.check_password(password):
                raise ValidationError({'password': 'Wrong Password'})
        return user


class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = User
        fields = ['email', 'full_name', 'password', 'password2']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_password2(self, value):
        data = self.get_initial()
        password = data.get('password')
        password2 = value
        if password != password2:
            raise ValidationError('Passwords must match')
        return value

    def save(self):
        user = User( email = self.validated_data['email'], full_name = self.validated_data['full_name'], )
        user.set_password(password)
        user.save()
        return user

class ChangePasswordSerializer(serializers.ModelSerializer):
    confirm = serializers.CharField(write_only=True)
    new = serializers.CharField(write_only=True)
    old = serializers.CharField(write_only=True)
    email = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'old', 'new' ,'confirm']

    def validate_confirm(self, value):
        data = self.get_initial()
        new = data.get('new')
        if new != value:
            raise ValidationError('Passwordos must match')
        return value

    def validate_old(self, value):
        data = self.get_initial()
        # email = data.get('email', None)
        user = User.objects.get(email=data.get('email', None))
        if user:
            if not user.check_password(value):
                raise ValidationError('Wrong password')
        return value

    def save(self, *args, **kwargs):
        data = self.get_initial()
        user = User.objects.get(email=data.get('email', None))
        user.set_password(data.get('new'))
        user.save()
        return user
