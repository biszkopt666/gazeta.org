from django.urls import path, include
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from .views import UserList, Logout, UserView, Login, SignUp, Activate, ChangePasswordView

urlpatterns = [
    path('login/', Login.as_view()),
    path('activate/<str:token>/', Activate.as_view()),
    path('delete/<str:token>/', Activate.as_view()),
    path('register/', SignUp.as_view()),
    path('userinfo/', UserView.as_view()),
    path('logout/', Logout.as_view()),
    path('userlist/', UserList.as_view()),
    path('changepassword/', ChangePasswordView.as_view()),
]
