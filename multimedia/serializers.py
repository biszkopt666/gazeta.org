from rest_framework import serializers
from .models import Photo
from django.conf import settings

class PhotoSerializer(serializers.ModelSerializer):

	def to_representation(self, instance):
		representation = super().to_representation(instance)
		representation['mini'] = self.context['request'].build_absolute_uri(instance.mini.url)
		return representation


	class Meta:
		model = Photo
		fields = ['id', 'original', 'category']
