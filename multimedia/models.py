from django.db import models
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys




class Photo(models.Model):
    category = models.CharField(max_length=150)
    original = models.ImageField(upload_to='photos' , blank=True, null=True)
    mini = models.ImageField(upload_to='photos', blank=True, null=True)

    def save(self, *args, **kwargs):

        image = Image.open(self.original)
        image = image.convert('RGB')

        thumbnail = Image.open(self.original)
        thumbnail = thumbnail.convert('RGB')

        image.thumbnail((900,900), Image.ANTIALIAS)

        width, height = image.size
        if width>400 and height>400:
            draw = ImageDraw.Draw(image)
            text = "GAZETA.ORG"
            font = ImageFont.truetype('arial.ttf', 36)
            textwidth, textheight = draw.textsize(text, font)
            margin = 10
            x = width - textwidth - margin
            y = height - textheight - margin
            draw.text((x, y), text, font=font)
        # image.show()
        output = BytesIO()
        image.save(output, format='JPEG', quality=75)
        output.seek(0)
        self.original= InMemoryUploadedFile(output,'ImageField', "%s" %self.original.name, 'image/jpeg', sys.getsizeof(output), None)

        if thumbnail.size[0] > thumbnail.size[1]:
            frame = int((thumbnail.size[0]-thumbnail.size[1])/2)
            thumbnail = thumbnail.crop((frame,0,thumbnail.size[1]+frame,thumbnail.size[1]))
        else:
            frame = int((thumbnail.size[1]-thumbnail.size[0])/2)
            thumbnail = thumbnail.crop((0,frame,thumbnail.size[0],thumbnail.size[1]-frame))
        thumbnail.thumbnail((300,300), Image.ANTIALIAS)
        output = BytesIO()
        thumbnail.save(output, format='JPEG', quality=75)
        output.seek(0)
        self.mini = InMemoryUploadedFile(output,'ImageField', "mini_%s" %self.original.name, 'image/jpeg', sys.getsizeof(output), None)

        super(Photo, self).save(*args, **kwargs)
