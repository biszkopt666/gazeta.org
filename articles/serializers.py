from rest_framework import serializers
from .models import Article
from accounts.serializers import UserSerializer
from django.conf import settings

class ArticleSerializer(serializers.HyperlinkedModelSerializer):

	def to_representation(self, instance):
		representation = super().to_representation(instance)
		representation['author'] = instance.author.full_name
		representation['slug'] = instance.slug
		representation['category'] = instance.get_category_display()
		representation['created'] = instance.pub_date
		user = self.context['request'].user
		if not user.is_anonymous:
			if instance.author.id is user.id or user.is_admin:
				representation['edit'] = True
		return representation

	class Meta:
		model = Article
		fields = ['id', 'title', 'content', 'category', 'cover', 'cover_mini']
