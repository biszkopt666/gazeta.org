from django.db.models import Q
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Article
from .serializers import ArticleSerializer
from rest_framework.response import Response
from rest_framework.pagination import LimitOffsetPagination
from .permissions import IsOwnerOrReadOnly, IsAdminOrReadOnly
from rest_framework.permissions import IsAuthenticatedOrReadOnly

class ArticlePagination(LimitOffsetPagination):
    default_limit = 20
    max_limit=100

class ArticleListView(ListCreateAPIView):
    serializer_class = ArticleSerializer
    authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly,]
    pagination_class = ArticlePagination

    def get_queryset(self):
        queryset = Article.objects.all().order_by('-pub_date')
        category = self.request.query_params.get('category', None)
        search = self.request.query_params.get('search', None)
        page = self.request.query_params.get('page', None)
        if category is not None:
            queryset = queryset.filter(category=category)
        if search is not None:
            queryset = queryset.filter(Q(title__icontains=search) | Q(content__icontains=search))
        return queryset

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class ArticleDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    permission_classes = [IsOwnerOrReadOnly, IsAdminOrReadOnly]
    serializer_class = ArticleSerializer
    lookup_field = 'slug'
