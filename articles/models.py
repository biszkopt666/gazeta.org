from django.db import models
from PIL import Image
from io import StringIO, BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys
from django.conf import settings
from django.utils.text import slugify
from random import randint

CATEGORY_CHOICES = [
    ('n','news'),
    ('c','culture'),
    ('s','sport'),
]

class Article(models.Model):
    title = models.CharField(max_length=150)
    slug = models.SlugField(max_length=150, unique=True, blank=True)
    content = models.TextField()
    category = models.CharField(max_length=1, choices=CATEGORY_CHOICES, default='n')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="author", on_delete=models.CASCADE, null=True)
    pub_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    cover = models.ImageField(upload_to='articles' , blank=True, null=True)
    cover_mini = models.ImageField(upload_to='articles', blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
            if Article.objects.filter(slug=self.slug).exists():
                self.slug+=str(randint(1,100))

        if self.cover:
            image = Image.open(self.cover)
            image = image.convert('RGB')
            image.thumbnail((900,900), Image.ANTIALIAS)
            output = BytesIO()
            image.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.cover= InMemoryUploadedFile(output,'ImageField', "%s" %self.cover.name, 'image/jpeg', sys.getsizeof(output), None)
            thumbnail = Image.open(self.cover)
            thumbnail = thumbnail.convert('RGB')
            if thumbnail.size[0] > thumbnail.size[1]:
                frame = int((thumbnail.size[0]-thumbnail.size[1])/2)
                thumbnail = thumbnail.crop((frame,0,thumbnail.size[1]+frame,thumbnail.size[1]))
            else:
                frame = int((thumbnail.size[1]-thumbnail.size[0])/2)
                thumbnail = thumbnail.crop((0,frame,thumbnail.size[0],thumbnail.size[1]-frame))
            thumbnail.thumbnail((300,300), Image.ANTIALIAS)
            output = BytesIO()
            thumbnail.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.cover_mini = InMemoryUploadedFile(output,'ImageField', "mini_%s" %self.cover.name, 'image/jpeg', sys.getsizeof(output), None)

        super(Article, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
