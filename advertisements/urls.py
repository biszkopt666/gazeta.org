from django.urls import path
from .views import CategoryListView, CategoryDetailView, AdvertisementListView, AdvertisementDetailView

urlpatterns = [
    path('', AdvertisementListView.as_view()),
    path('<str:slug>', AdvertisementDetailView.as_view()),
    path('category/', CategoryListView.as_view()),
    path('category/<str:slug>', CategoryDetailView.as_view()),
]
