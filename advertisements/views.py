from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from django.db.models import Q
from .models import Category, Advertisement
from .serializers import CategorySerializer, AdvertisementSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.pagination import LimitOffsetPagination

class AdvertisementPagination(LimitOffsetPagination):
    default_limit = 20
    max_limit=100

class CategoryListView(ListCreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly, ]
    # def get_queryset(self):

class CategoryDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly, ]
    lookup_field = 'slug'

class AdvertisementListView(ListCreateAPIView):
    serializer_class = AdvertisementSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, ]
    pagination_class = AdvertisementPagination

    def get_queryset(self):
        queryset = Advertisement.objects.all().order_by('-pub_date')
        item = self.request.query_params.get('item', None)
        category = self.request.query_params.get('category', None)
        price = self.request.query_params.get('price', None)
        if price is not None:
            queryset = queryset.filter(price__range=(0, price))
        if category is not None:
            queryset = queryset.filter(category__slug=category)
        if item is not None:
            queryset = queryset.filter(Q(title__icontains=item) | Q(content__icontains=item))
        return queryset

class AdvertisementDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = AdvertisementSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, ]
    queryset = Advertisement.objects.all()
    lookup_field = 'slug'
