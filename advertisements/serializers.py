from rest_framework import serializers
from .models import Advertisement, Category

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['name', 'slug', 'id']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['slug'] = instance.slug
        return representation

class AdvertisementSerializer(serializers.ModelSerializer):
    category = serializers.SlugRelatedField( slug_field='slug', queryset=Category.objects.all())

    class Meta:
        model = Advertisement
        fields = ['title', 'content', 'category', 'price']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['slug'] = instance.slug
        representation['category_name'] = instance.category.name
        representation['created'] = instance.pub_date
        return representation
