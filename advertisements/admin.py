from django.contrib import admin
from .models import Advertisement, Category
from django.contrib import admin

admin.site.register(Advertisement)
admin.site.register(Category)
