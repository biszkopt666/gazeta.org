from django.db import models
from django.utils.text import slugify
from random import randint

class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
            if Category.objects.filter(slug=self.slug).exists():
                print('exists')
                self.slug+=str(randint(1,100))
            super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

class Advertisement(models.Model):
    title = models.CharField(max_length=150)
    slug = models.SlugField(max_length=150, unique=True, blank=True)
    content = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    price = models.FloatField(default=100)
    pub_date = models.DateTimeField(auto_now_add=True, auto_now=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
            if Advertisement.objects.filter(slug=self.slug).exists():
                self.slug+=str(randint(1,100))
        super(Advertisement, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
